<section class="content-header">
  
</section>


    <section class="content">
      <div class="row">
        <div class="col-12">
            <div class="card card-primary">
              <div class="card-header">
                <div class="card-title">
                 Jadwal Studio rekaman
                </div>
              <div class="btn-group float-sm-right" >
         <!--       <button style="background-color: #5c6978; border-radius: 8px;"  type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-rekaman">
                  <h6 style="color: #ffffff;">Sewa Studio rekaman</h6>
                </button> -->
                <button type="button" class="btn btn-block bg-gradient-success" data-toggle="modal" data-target="#modal-rekaman"> Sewa Studio rekaman</button>
             </div>
            </div>
             </div>

<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="box">
            <div class="box-header">
            </div>
            <!-- /.box-header -->
             <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <div id="calendar-container" style="padding: 100px;">
                 <div id="calendar"></div>
            </div>
          </table>
        </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

 <?php foreach ($jadwalstudiorekaman as $key1 => $value1) { ?>
      <div class="modal fade" id="edit_<?php echo $value1->id_studiorekaman ?>">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Tambahkan Ruangan Studio rekaman</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form action="<?php echo $action1 ?>" method="post">
            <div class="modal-body">
              <div class="form-group">
                <label for="exampleInputEmail1"></label>
                <input type="text" name="nama_studio" id="nama_studio" class="form-control" placeholder="Masukkan Ruangan Studio rekaman" required autocomplete="off" value="<?php echo $value1->nama_studio ?>" />
                <input type="hidden" name="id_studiorekaman" id="nama_studio" class="form-control" value="<?php echo $value1->id_studiorekaman; ?>" /> 
              </div>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
              <button type="submit" class="btn btn-primary">Edit</button>
            </div>
            </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
        <?php } ?>
      <div class="modal fade" id="modal-rekaman">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Pilih Studio rekaman</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form action="<?php echo base_url('jadwalstudiorekaman/create_action') ?>" method="post">
            <div class="modal-body">
              <label style="width:300px">Masukkan Tanggal</label>
                <input type="date" name="tanggal" min='<?php echo date('Y-m-d'); ?>'  id="tanggal" class="custom-select form-control" style="width:300px" placeholder="Masukkan Tanggal" required autocomplete="off" />
            </div>
            <div class="modal-body">
              <label style="width:300px">Pilih Nama Studio rekaman</label>
              <select  name="id_studiorekaman" id="id_studiorekaman" class="custom-select form-control" style="width:300px"  required>

              <option value="" diselected>-- Pilih Studio rekaman --</option> 
                <?php foreach ($studiom as $row) { echo "<option value='".$row->id_studiorekaman."'>".$row->nama_studio."</option>"; } ?>

             </select>
           </div>
             <!-- <label style="width:300px">Pilih Sesi Studio rekaman</label>
             <select  name="id_sesistudiorekaman" id="id_sesistudiorekaman" class="custom-select form-control" style="width:300px"  required>
              <option value="" diselected>-- Pilih Studio Sesi Studio rekaman --</option> 
                <?php foreach ($sesistudiom as $row) { echo "<option value='".$row->id_sesistudiorekaman."'>".$row->namasesi."</option>"; } ?>

             </select> -->
             <div class="modal-body">
             <label style="width:300px">Pilih Sesi Studio rekaman</label>
             <br>
              <select  name="id_sesistudiorekaman" id="id_sesistudiorekaman" class="custom-select form-control" style="width:300px"  required>
              <option value="" diselected>-- Pilih Sesi Studio rekaman --</option> 
             <?php foreach ($sesistudiom as $row) { 
                echo "<option value='".$row->id_sesistudiorekaman."'>".$row->namasesi."</option>";
                // echo "<br>";
                // echo "<div class='form-group'>";
                // echo "<div class='form-check'>";
                // echo "<input class='form-check-input' type='checkbox' name='id_sesistudiorekaman[]' value='".$row->id_sesistudiorekaman."'>";
                // echo "<label class='form-check-label'>".$row->namasesi."</label>";
                // echo "</div>";
                // echo "</div>";
                // echo "<option value='".$row->id_sesistudiorekaman."'>".$row->namasesi."</option>"; 
              } ?>
            </select>
          </div>
              <br>
              

            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary" disabled="" id="tambah-tombol">Tambah</button>
            </div>
          </div>
        </form>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>



