<DOCTYPE html>
	<html>
  <head>
	  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>BUKTI SEWA STUDIO MUSIK</title>
  <link rel="stylesheet" href="">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <style>

    table {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  margin-left: 20px;
    margin-right: 20px;
}
tr,td{
  padding : 5px;
}

  </style>
</head>
<body>
  <img src="./img/digage.png" style="position: absolute; width: 60px; height: auto;">
  <table style="width: 100%;">
    <tr>
      <td align="center">
        <span style="line-height: 1.6; font-weight: bold;">
          STUDIO MUSIK DIGAGE
          <br>Jl. Solo-Sukoharjo Jl. Dompilan I, Gabusan, Jombor, Kec. Bendosari, Kabupaten Sukoharjo, Jawa Tengah 57521
        </span>
      </td>
    </tr>
  </table>
   <hr class="line-title"> 
  <p align="center">
    BUKTI SEWA STUDIO REKAMAN <br>
    <b>*harap tunjukkan bukti ini ke bagian admin studio rekaman digage</b>
  </p>
    <body>
      <div style="overflow-x:auto;">
    	<table >

              
                      <tr>
                        <td>Nama Pemesan</td>
                       	<td align="right"><?php echo $namapemesan?></td>
                    </tr>
                    <tr>
                        <td>Nama Studio</td>
                        <td align="right"><?php echo $nama_studio?></td>
                    </tr>
                      <tr>
                        <td>Tanggal Pemesan</td>
                        <td align="right"><?php echo $tanggal?></td>
                    </tr>
                    <tr>
                        <td>Sesi</td>
                        <td align="right"><?php echo $namasesi?></td>
                    </tr>
                    <tr>
                        <td>Jam Mulai</td>
                        <td align="right"><?php echo $jammulai?></td>
                    </tr>
                    <tr>
                        <td>Jam Selesai</td>
                        <td align="right"><?php echo $jamakhir?></td>
                    </tr>
                    <tr>
                        <td>Fasilitas</td>
                        <td align="right"><?php echo $fasilitas?></td>
                    </tr>
                    
                     <br/>
                      
    	</table>
      <hr>
      <table >   <tr>
                        <td>Total Harga</td>
                        <td align="right">Rp <?php echo $total_harga?></td>
                    </tr>
                    <tr>
                        <td>Status Pembayaran</td>
                        <td align="right"><?php echo $keterangan?></td>
                    </tr>
      </table>
    </div>
    </body>
    </html>
    