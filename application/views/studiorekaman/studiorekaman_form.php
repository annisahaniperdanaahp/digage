<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Studio Rekaman</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="<?php echo base_url('studiorekaman/update_action') ?>" method="post">
                <div class="card-body">
                  <div class="form-group">
                      <label for="exampleInputEmail1"></label>
                      <input type="text" name="nama_studio" value="<?php echo $studiorekaman ?>" class="form-control" placeholder="Masukkan Ruangan Studio Rekaman" required>
                      <input type="hidden" name="id_studiorekaman" value="<?php echo $id_studiorekaman ?>" class="form-control">
                  </div>
                  </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary"><?php echo $title ?></button>
                  <a href="<?php echo site_url('studiorekaman') ?>" class="btn btn-default">Batal</a>
                </div>
              </form>
            </div>

