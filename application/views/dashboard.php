    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url('dashboard') ?>">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-6 col-12">
              <?php foreach ($studiomusik as $key => $value) {
               ?>
            <div class="card">
                 <div class="row">
          <div class="col-lg-12 col-12">
              <div class="card-header border-0">
                <h3 class="card-title">Studio Musik <?php echo $value['tanggal'] ?></h3>
                </div>
              <div class="card-body p-0">
                <table class="table table-striped table-valign-middle">
                  <thead>
                  <tr>
                    <th style=" background-color: #4CAF50;">Ruang</th>
                    <th style=" background-color: #4CAF50;">Sesi</th>
                    <th style=" background-color: #4CAF50;">Jam Mulai</th>
                    <th style=" background-color: #4CAF50;">Jam Selesai</th>
                    <th style=" background-color: #4CAF50;">Status</th>
                  </tr>
                  </thead>
                  <tbody>
                      <?php foreach ($value['data'] as $key => $v) {?>
                  <tr>
                    <td>
                     <?= $v['ruang'] ?>
                    </td>
                    <td>
                     <?= $v['sesi'] ?>
                    </td>
                    <td>
                     <?= $v['jammulai'] ?>
                    </td>
                    <td>
                     <?= $v['jamakhir'] ?>
                    </td>
                    <td>
                     Terpakai
                    </td>
                  </tr>
                      <?php }?>
                  </tbody>
                </table>
              </div>
            </div>
            </div>
            </div>
              <?php }?>
            </div>
          <div class="col-lg-6 col-12">
            <?php foreach ($studiorekaman as $key => $value) {
               ?>
            <div class="card">
                 <div class="row">
          <div class="col-lg-12 col-12">
              <div class="card-header border-0">
                <h3 class="card-title">Studio Rekaman <?php echo $value['tanggal'] ?></h3>
                </div>
              <div class="card-body p-0">
                <table class="table table-striped table-valign-middle">
                  <thead>
                   <tr>
                    <th style=" background-color: red;">Ruang</th>
                    <th style=" background-color: red;">Sesi</th>
                    <th style=" background-color: red;">Jam Mulai</th>
                    <th style=" background-color: red;">Jam Selesai</th>
                    <th style=" background-color: red;">Status</th>
                  </tr>
                  </thead>
                  <tbody>
                      <?php foreach ($value['data'] as $key => $v) {?>
                  <tr>
                    <td>
                     <?= $v['ruang'] ?>
                    </td>
                    <td>
                     <?= $v['sesi'] ?>
                    </td>
                    <td>
                     <?= $v['jammulai'] ?>
                    </td>
                    <td>
                     <?= $v['jamakhir'] ?>
                    </td>
                  </tr>
                      <?php }?>
                  </tbody>
                </table>
              </div>
            </div>
            </div>
            </div>
              <?php }?>
            </div>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->