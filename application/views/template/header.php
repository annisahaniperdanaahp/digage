<link rel="stylesheet" href="<?php echo base_url('assets/') ?>plugins/fontawesome-free/css/all.min.css">
<!-- Tempusdominus Bbootstrap 4 -->
<link rel="stylesheet" href="<?php echo base_url('assets/') ?>plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
<link rel="stylesheet" href="<?php echo base_url('assets/') ?>plugins/fontawesome-free/css/all.min.css">
<!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url('assets/') ?>plugins/datatables-bs4/css/dataTables.bootstrap4.css">
<!-- iCheck -->
<link rel="stylesheet" href="<?php echo base_url('assets/') ?>plugins/icheck-bootstrap/icheck-bootstrap.min.css">
<!-- JQVMap -->
<link rel="stylesheet" href="<?php echo base_url('assets/') ?>plugins/jqvmap/jqvmap.min.css">
 <!-- SweetAlert2 -->
 <link rel="stylesheet" href="<?php echo base_url('assets/') ?>plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="<?php echo base_url('assets/') ?>dist/css/adminlte.min.css">
<!-- overlayScrollbars -->
<link rel="stylesheet" href="<?php echo base_url('assets/') ?>plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
<!-- Daterange picker -->
<link rel="stylesheet" href="<?php echo base_url('assets/') ?>plugins/daterangepicker/daterangepicker.css">
<!-- summernote -->
<link rel="stylesheet" href="<?php echo base_url('assets/') ?>plugins/summernote/summernote-bs4.css">
 <!-- Toastr -->
  <link rel="stylesheet" href="<?php echo base_url('assets/') ?>plugins/toastr/toastr.min.css">

<link rel="stylesheet" href="<?php echo base_url('assets/fullcalendar/packages/core/main.css') ?>">
<link rel="stylesheet" href="<?php echo base_url('assets/fullcalendar/packages/daygrid/main.css') ?>">
<link rel="stylesheet" href="<?php echo base_url('assets/fullcalendar/packages/timegrid/main.css') ?>">
<link rel="stylesheet" href="<?php echo base_url('assets/fullcalendar/packages/list/main.css') ?>">
  
<link rel="shortcut icon" href="<?php echo base_url();?>./img/digage.jpeg">