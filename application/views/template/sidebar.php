<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="./img/digage.jpeg" alt="DIGAGE" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">DIGAGE</span>
    </a>
     <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="./img/user.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <p><font color='#ffffff'><?php echo $this->session->userdata('nama'); ?></font></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

          <li class="nav-item">
            <a href="<?php echo site_url('dashboard') ?>" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                DASHBOARD
                <span class="right badge badge-danger"></span>
              </p>
            </a>
          </li>

<!-- //studiomusik -->
           <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-music"></i>
              <p>STUDIO MUSIK</p>
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>

            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo site_url('studiomusik') ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>List Studio</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo site_url('sesistudiomusik') ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Sesi Studio</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo site_url('peminjamanstudiomusik') ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Riwayat Penyewaan Studio</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo site_url('jadwalstudiomusik') ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Jadwal Studio</p>
                </a>
              </li>
            </ul>
<!-- //rekamanmusik -->
           <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-microphone"></i>
              <p>REKAMAN MUSIK</p>
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>

            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo site_url('studiorekaman') ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>List Rekaman</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo site_url('sesistudiorekaman') ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Sesi Rekaman</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo site_url('peminjamanstudiorekaman') ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Riwayat Penyewaan Rekaman</p>
                </a>
              <li class="nav-item">
                <a href="<?php echo site_url('jadwalstudiorekaman') ?>"class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Jadwal Rekaman</p>
                </a>
              </li>
            </ul>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>