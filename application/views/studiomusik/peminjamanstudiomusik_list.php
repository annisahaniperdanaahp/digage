<section class="content-header">
<!--   <h1>
    <?php echo $title ?>
  </h1> -->
</section>

<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
            <div class="card card-primary">
              <div class="card-header">
                <div class="card-title">
                 Riwayat Penyewaan Studio Musik
                </div>
                <div class="btn-group float-sm-right">
<!--               <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-musik">
                  Sewa Studio Musik
                </button> -->
             </div>
              </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th><center>No</center></th>
                    <th><center>Tanggal</center></th>
                    <th><center>Nama Studio</center></th>
                    <th><center>Sesi</center></th>
                    <th><center>Harga Sewa</center></th>
                    <th><center>Status</center></th>
                    <th><center>Aksi</center></th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                  $no=0;
                    foreach ($peminjamanstudiomusik as $key => $value) { 
                      ?>
                      <tr>
                        <td><?php echo ++$no ?></td>
                       <td><?php echo $value->tanggal?></td>
                        <td><?php echo $value->nama_studio ?></td>
                        <td><?php echo $value->namasesi ?></td>
                        <td><?php echo $value->harga ?></td>
                        <td>
                          <?php if($value->status == 'Disewa'){ ?>
                          <center><span class="badge bg-primary" style="width: 130px; height: 25px; font-size: 15px; text-align: center;"><?php echo $value->status ?></span></center>
                          <?php } else if($value->status == 'Sewa Dibatalkan'){ ?>
                            <center><span class="badge bg-danger" style="width: 130px; height: 25px; font-size: 15px; text-align: center;"><?php echo $value->status ?></span></center>
                          <?php }elseif($value->status == 'Berhasil'){?>
                            <center><span class="badge bg-success" style="width: 130px; height: 25px; font-size: 15px; text-align: center;"><?php echo $value->status ?></span></center>
                          <?php }else{ ?>
                            <center><span class="badge bg-warning" style=" height: 25px; font-size: 15px; text-align: center;"><?php echo $value->status ?></span></center>
                          <?php } ?>
                        </td>

                      
                        <td>
                      <center>
                          <?php if($value->status == 'Berhasil') {?>
                            <a class="btn btn-primary" href=" <?php echo base_url    ('peminjamanstudiomusik/pdf/'.$value->id_transaksistudiomusik) ?>" target='_blank'> <i class="fa fa-print"></i>Bukti Sewa</a>
                            <?php }elseif($value->status != 'Sewa Dibatalkan') {?>
                            <?php $tgl2 = date('Y-m-d 00:00:00', strtotime('-1 days', strtotime($value->tanggal)));?>
                            <?php if($tgl2 >= date('Y-m-d H:i:s', time())) {?>
                               <?php if($value->status == 'Menunggu Konfirmasi Pembatalan') {?>
                             <a class="btn btn-primary" href=" <?php echo base_url    ('peminjamanstudiomusik/pdf/'.$value->id_transaksistudiomusik) ?>" target='_blank'> <i class="fa fa-print"></i>Bukti Sewa</a>
                               <?php } else {?>
                                <a type="button" class="btn btn-secondary" href="<?php echo site_url('peminjamanstudiomusik/cancel/'.$value->id_peminjamanstudiomusik) ?>">
                                        Batalkan Sewa
                                    </a>
                               <a class="btn btn-primary" href=" <?php echo base_url    ('peminjamanstudiomusik/pdf/'.$value->id_transaksistudiomusik) ?>" target='_blank'> <i class="fa fa-print"></i>Bukti Sewa</a>
                               <?php } ?>
                            <?php }else{?>
                              <a class="btn btn-primary" href=" <?php echo base_url    ('peminjamanstudiomusik/pdf/'.$value->id_transaksistudiomusik) ?>" target='_blank'> <i class="fa fa-print"></i>Bukti Sewa</a>
                            <?php }?> 
                          <?php }?>   
                          </center>
                        </td>
                      </tr>

                  <?php
                    }
                  ?>
                  
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->



      <?php foreach ($peminjamanstudiomusik as $key1 => $value1) { ?>
      <div class="modal fade" id="edit_<?php echo $value1->id_studiomusik ?>">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Tambahkan Ruangan Studio Musik</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form action="<?php echo $action1 ?>" method="post">
            <div class="modal-body">
              <div class="form-group">
                <label for="exampleInputEmail1"></label>
                <input type="text" name="nama_studio" id="nama_studio" class="form-control" placeholder="Masukkan Ruangan Studio Musik" required autocomplete="off" value="<?php echo $value1->nama_studio ?>" />
                <input type="hidden" name="id_studiomusik" id="nama_studio" class="form-control" value="<?php echo $value1->id_studiomusik; ?>" /> 
              </div>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
              <button type="submit" class="btn btn-primary">Edit</button>
            </div>
            </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
        <?php } ?>
      <div class="modal fade" id="modal-musik">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Pilih Studio Musik</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form action="<?php echo base_url('peminjamanstudiomusik/create_action') ?>" method="post">
            <div class="modal-body">
              <label style="width:300px">Masukkan Tanggal</label>
                <input type="date" name="tanggal" id="tanggal" class="custom-select form-control" style="width:300px" placeholder="Masukkan Tanggal" required autocomplete="off" />
            </div>
              <label style="width:300px">Pilih Nama Studio Musik</label>
              <select  name="id_studiomusik" id="id_studiomusik" class="custom-select form-control" style="width:300px"  required>

              <option value="" diselected>-- Pilih Studio Musik --</option> 
                <?php foreach ($studiom as $row) { echo "<option value='".$row->id_studiomusik."'>".$row->nama_studio."</option>"; } ?>

             </select>
             <!-- <label style="width:300px">Pilih Sesi Studio Musik</label>
             <select  name="id_sesistudiomusik" id="id_sesistudiomusik" class="custom-select form-control" style="width:300px"  required>
              <option value="" diselected>-- Pilih Studio Sesi Studio Musik --</option> 
                <?php foreach ($sesistudiom as $row) { echo "<option value='".$row->id_sesistudiomusik."'>".$row->namasesi."</option>"; } ?>

             </select> -->

             <label style="width:300px">Pilih Sesi Studio Musik</label>
             <br>
             <?php foreach ($sesistudiom as $row) { 
                echo "<input type='checkbox'  name='id_sesistudiomusik[]' value='".$row->id_sesistudiomusik."'>".$row->namasesi."";
                // echo "<br>";
                // echo "<div class='form-group'>";
                // echo "<div class='form-check'>";
                // echo "<input class='form-check-input' type='checkbox' name='id_sesistudiomusik[]' value='".$row->id_sesistudiomusik."'>";
                // echo "<label class='form-check-label'>".$row->namasesi."</label>";
                // echo "</div>";
                // echo "</div>";
                // echo "<option value='".$row->id_sesistudiomusik."'>".$row->namasesi."</option>"; 
              } ?>
              <br>
              

            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Tambah</button>
            </div>
          </div>
        </form>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>



