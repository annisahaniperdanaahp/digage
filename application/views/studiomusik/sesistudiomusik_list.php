<section class="content-header">
    <?php echo $title ?>
  </h1> -->
</section>

<section class="content">

      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card card-primary">
              <div class="card-header">
                <div class="card-title">
                 Sesi Studio Musik
                </div>
                <div class="btn-group float-sm-right">
             </div>
              </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Sesi</th>
                    <th>Jam Mulai</th>
                    <th>Jam Akhir</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                  $no=0;
                    foreach ($sesistudiomusik as $key => $value) { ?>
                      <tr>
                        <!-- <?php 
                              $waktu = $value->jam;
                              $time = strtotime($waktu);
                         ?> -->
                        <td><?php echo ++$no ?></td> 
                         <td><?php echo $value->namasesi ?></td>
                          <td><?php echo $value->jammulai ?></td>
                           <td><?php echo $value->jamakhir ?></td>
                      </tr>

                  <?php
                    }

                  ?>
                  
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->


        <?php foreach ($sesistudiomusik as $key1 => $value1) { ?>
      <div class="modal fade" id="edit_<?php echo $value1->id_sesistudiomusik ?>">
        <div class="modal-dialog">
          <div class="modal-content">
                     <div class="modal-header">
              <h4 class="modal-title">Sesi Studio Musik</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form action="<?php echo base_url('sesistudiomusik/create_action') ?>" method="post">
            <div class="modal-body">
              <label for="exampleInputEmail1">Nama Sesi</label>
              <input type="text" name="namasesi" value="" class="form-control" placeholder="Masukkan Ruangan Studio Musik" required>
              <input type="hidden" name="id_sesistudiomusik" value="" class="form-control">

              <label for="exampleInputEmail1">Jam Awal</label>
              <input type="time" name="jammulai" value="" class="form-control" placeholder="Masukkan Jam Awal" required>
              <input type="hidden" name="id_sesistudiomusik" value="" class="form-control">

              <label for="exampleInputEmail1">Jam Akhir</label>
              <input type="time" name="jamakhir" value="" class="form-control" placeholder="Masukkan Jam Akhir" required>
              <input type="hidden" name="id_sesistudiomusik" value="" class="form-control">

            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
              <button type="submit" class="btn btn-primary">Edit</button>
            </div>
            </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
        <?php } ?>
      <div class="modal fade" id="modal-musik">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Sesi Studio Musik</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form action="<?php echo base_url('sesistudiomusik/create_action') ?>" method="post">
            <div class="modal-body">
              <label for="exampleInputEmail1">Nama Sesi</label>
              <input type="text" name="namasesi" value="" class="form-control" placeholder="Masukkan Ruangan Studio Musik" required>
              <input type="hidden" name="id_sesistudiomusik" value="" class="form-control">

              <label for="exampleInputEmail1">Jam Awal</label>
              <input type="time" name="jammulai" value="" class="form-control" placeholder="Masukkan Jam Awal" required>
              <input type="hidden" name="id_sesistudiomusik" value="" class="form-control">

              <label for="exampleInputEmail1">Jam Akhir</label>
              <input type="time" name="jamakhir" value="" class="form-control" placeholder="Masukkan Jam Akhir" required>
              <input type="hidden" name="id_sesistudiomusik" value="" class="form-control">

            </div>

            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Tambah</button>
            </div>
          </div>
        </form>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>