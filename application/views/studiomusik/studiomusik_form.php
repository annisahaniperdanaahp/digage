<div class="row justify-content-center">
<div class="col-4 align-self-center">
<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Studio Musik</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              
              <form role="form" action="<?php echo base_url('studiomusik/update_action') ?>" method="post">
                <div class="card-body">
                  <div class="form-group">
                      <label for="exampleInputEmail1"></label>
                      <input type="text" name="nama_studio" value="<?php echo $studiomusik ?>" class="form-control" placeholder="Masukkan Ruangan Studio Musik" required>
                      <input type="hidden" name="id_studiomusik" value="<?php echo $id_studiomusik ?>" class="form-control">
                  </div>
                  </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary"><?php echo $title ?></button>
                  <a href="<?php echo site_url('studiomusik') ?>" class="btn btn-default">Batal</a>
                </div>
              </form>
            </div>
            </div>
          </div>

