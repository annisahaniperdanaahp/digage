<?php
class Template {
	protected $_ci;
	function __construct(){
		$this->_ci=&get_instance();
	}

	function display($template,$data=null){

		$data['_header']	= $this->_ci->load->view('template/header',		$data, true);
		$data['_navbar']	= $this->_ci->load->view('template/navbar',		$data, true);
		$data['_sidebar']	= $this->_ci->load->view('template/sidebar',	$data, true);
		$data['_content']	= $this->_ci->load->view($template, $data, true);
		
		$this->_ci->load->view('template/template.php', $data);
	}

	function login($data=null){
		$data['_header']	= $this->_ci->load->view('template/header', $data, true);
		$data['_content']	= $this->_ci->load->view('template/login',	$data, true);

		$this->_ci->load->view('template/login.php', $data);
	}
}