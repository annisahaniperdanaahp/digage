<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class sesistudiorekaman_model extends CI_Model{
    function __construct() {
        parent::__construct();
    }

    function get_all() {
        return $this->db->get('sesistudiorekaman')->result();    
    }

    function get_by($id_sesistudiorekaman) {
        $this->db->where('id_sesistudiorekaman', $id_sesistudiorekaman);
        return $this->db->get('sesistudiorekaman')->row();    
    }

    function create($data) {
        $this->db->insert('sesistudiorekaman', $data);
        return $this->db->get('sesistudiorekaman')->row();
    }

    function update($id_sesistudiorekaman, $data) {
        $this->db->where('id_sesistudiorekaman', $id_sesistudiorekaman);
        $this->db->update('sesistudiorekaman', $data);
    }

    function delete($id_sesistudiorekaman) {
        $this->db->where('id_sesistudiorekaman', $id_sesistudiorekaman);
        $this->db->delete('sesistudiorekaman');
    }
    
}
?>