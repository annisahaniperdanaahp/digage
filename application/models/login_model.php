<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class login_model extends CI_Model{
    function __construct() {
        parent::__construct();
    }

    function cek_login($email, $password) {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('email', $email);
        $this->db->where('password', $password);
        $query = $this->db->get(); 
        return $query;
    }

}
