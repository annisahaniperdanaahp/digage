<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class jadwalstudiomusik_model extends CI_Model{
    function __construct() {
        parent::__construct();
    }

    function get_all() {
        $this->db->join('studiomusik','studiomusik.id_studiomusik = jadwalstudiomusik.id_studiomusik');   
        $this->db->join('sesistudiomusik','sesistudiomusik.id_sesistudiomusik = jadwalstudiomusik.id_sesistudiomusikjoin');
        $this->db->join('user','user.id_user = jadwalstudiomusik.id_user');
        return $this->db->get('jadwalstudiomusik')->result(); 

    }

    function eventlist() {
        $this->db->join('studiomusik','studiomusik.id_studiomusik = peminjamanstudiomusik.id_studiomusik');   
        $this->db->join('sesistudiomusik','sesistudiomusik.id_sesistudiomusik = peminjamanstudiomusik.id_sesistudiomusik');
        $this->db->join('user','user.id_user = peminjamanstudiomusik.id_user');
        return $this->db->get('peminjamanstudiomusik'); 

    }
    function cek($tanggal,$id_studiomusik,$id_sesistudiomusik) {
        $this->db->join('studiomusik','studiomusik.id_studiomusik = peminjamanstudiomusik.id_studiomusik');   
        $this->db->join('sesistudiomusik','sesistudiomusik.id_sesistudiomusik = peminjamanstudiomusik.id_sesistudiomusik');
        $this->db->join('user','user.id_user = peminjamanstudiomusik.id_user');
        $this->db->like('peminjamanstudiomusik.tanggal',$tanggal);
        $this->db->where('peminjamanstudiomusik.id_studiomusik',$id_studiomusik);
        $this->db->where('peminjamanstudiomusik.id_sesistudiomusik',$id_sesistudiomusik);
        $this->db->where('peminjamanstudiomusik.status !=',"Sewa Dibatalkan");

        return $this->db->get('peminjamanstudiomusik')->row(); 

    }

     function create($data) {
        $this->db->insert('peminjamanstudiomusik', $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;

    }

    function create_t($data2) {
        $this->db->insert('transaksistudiomusik', $data2);
        return $this->db->get('transaksistudiomusik')->row();

    }


    function get_by($id_jadwalstudiomusik) {
        $this->db->where('id_jadwalstudiomusik', $id_jadwalstudiomusik);
        return $this->db->get('jadwalstudiomusik')->row();    
    }

    function harga($id_studiomusik) {
        $this->db->where('id_studiomusik', $id_studiomusik);
        return $this->db->get('studiomusik')->row()->harga;    
    }

    function jammulai($id_sesistudiomusik) {
        $this->db->where('id_sesistudiomusik', $id_sesistudiomusik);
        return $this->db->get('sesistudiomusik')->row()->jammulai;    
    }

    function studiomusik() {
        return $this->db->get('studiomusik')->result();
    }

    function get_sesi() {
        return $this->db->get('sesistudiomusik')->result();    
    }

    
}
?>