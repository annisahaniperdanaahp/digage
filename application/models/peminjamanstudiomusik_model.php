<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class peminjamanstudiomusik_model extends CI_Model{
    function __construct() {
        parent::__construct();
    }
    function get_all_transaksi($id_user) { 
        $this->db->join('peminjamanstudiomusik','peminjamanstudiomusik.id_peminjamanstudiomusik=transaksistudiomusik.id_peminjamanstudiomusik');
        $this->db->join('studiomusik','studiomusik.id_studiomusik=peminjamanstudiomusik.id_studiomusik');
        $this->db->join('sesistudiomusik','sesistudiomusik.id_sesistudiomusik=peminjamanstudiomusik.id_sesistudiomusik');
        $this->db->join('user','user.id_user=peminjamanstudiomusik.id_user');
        $this->db->where('peminjamanstudiomusik.id_user', $id_user);
         $this->db->order_by('peminjamanstudiomusik.tanggal','DESC');
        return $this->db->get('transaksistudiomusik')->result();    
    }
    function get_all($id_user) {
        $this->db->join('studiomusik','studiomusik.id_studiomusik=peminjamanstudiomusik.id_studiomusik');
        $this->db->join('sesistudiomusik','sesistudiomusik.id_sesistudiomusik=peminjamanstudiomusik.id_sesistudiomusik');
        $this->db->join('user','user.id_user=peminjamanstudiomusik.id_user');
        $this->db->where('peminjamanstudiomusik.id_user', $id_user);
        return $this->db->get('peminjamanstudiomusik')->result();    
    }

    function get_by($id_peminjamanstudiomusik) {
        $this->db->where('id_peminjamanstudiomusik', $id_peminjamanstudiomusik);
        return $this->db->get('peminjamanstudiomusik')->row();    
    }

    function harga($id_peminjamanstudiomusik) {
        $this->db->where('id_peminjamanstudiomusik', $id_peminjamanstudiomusik);
        return $this->db->get('peminjamanstudiomusik')->row()->harga;    
    }

    
 
    function create($data) {
        $this->db->insert('peminjamanstudiomusik', $data);
        return $this->db->get('peminjamanstudiomusik')->row()->insert_id();

    }

    function create_t($data2) {
        $this->db->insert('transaksistudiomusik', $data2);
        return $this->db->get('transaksistudiomusik')->row();

    }

    function studiomusik() {
        return $this->db->get('studiomusik')->result();
    }

    function sesistudiomusik() {
        return $this->db->get('sesistudiomusik')->result();
    }

    function update($id_peminjamanstudiomusik, $data) {
        $this->db->where('id_peminjamanstudiomusik', $id_peminjamanstudiomusik);
        $this->db->update('peminjamanstudiomusik', $data);
    }

    function cancel($id_peminjamanstudiomusik, $data) {
        $this->db->where('id_peminjamanstudiomusik', $id_peminjamanstudiomusik);
        $this->db->update('peminjamanstudiomusik', $data);
    }

    function delete($id_peminjamanstudiomusik) {
        $this->db->where('id_peminjamanstudiomusik', $id_peminjamanstudiomusik);
        $this->db->delete('peminjamanstudiomusik');
    }

      function tampil_data() {
        $this->db->join('studiomusik','studiomusik.id_studiomusik=peminjamanstudiomusik.id_studiomusik');
        $this->db->join('sesistudiomusik','sesistudiomusik.id_sesistudiomusik=peminjamanstudiomusik.id_sesistudiomusik');
         $this->db->join('user','user.id_user=peminjamanstudiomusik.id_user');
        return $this->db->get('peminjamanstudiomusik');
    }
    function cetak($id, $id_user){
        $this->db->join('peminjamanstudiomusik','peminjamanstudiomusik.id_peminjamanstudiomusik=transaksistudiomusik.id_peminjamanstudiomusik');
        $this->db->join('studiomusik','studiomusik.id_studiomusik=peminjamanstudiomusik.id_studiomusik');
        $this->db->join('sesistudiomusik','sesistudiomusik.id_sesistudiomusik=peminjamanstudiomusik.id_sesistudiomusik');
        $this->db->join('user','user.id_user=peminjamanstudiomusik.id_user');
        $this->db->where('transaksistudiomusik.id_transaksistudiomusik', $id);
        $this->db->where('peminjamanstudiomusik.id_user', $id_user);
        return $this->db->get('transaksistudiomusik')->result();    
    }
}
?>