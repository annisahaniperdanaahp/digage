<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class studiorekaman_model extends CI_Model{
    function __construct() {
        parent::__construct();
    }

    function get_all() {
        // $this->db->where('peminjamanstudiorekaman.id_user', $id_user);
        return $this->db->get('studiorekaman')->result(); 
    }

    function get_by($id_studiorekaman) {
        $this->db->where('id_studiorekaman', $id_studiorekaman);
        return $this->db->get('studiorekaman')->row();    
    }

    function create($data) {
        $this->db->insert('studiorekaman', $data);
        return $this->db->get('studiorekaman')->row();
    }

    function update($id_studiorekaman, $data) {
        $this->db->where('id_studiorekaman', $id_studiorekaman);
        $this->db->update('studiorekaman', $data);
    }

    function delete($id_studiorekaman) {
        $this->db->where('id_studiorekaman', $id_studiorekaman);
        $this->db->delete('studiorekaman');
    }
    
}
?>