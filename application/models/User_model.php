<?php
 class User_model extends CI_Model{
  public function register($enc_password){
   // User data array
   $data = array(
    'nama' => $this->input->post('nama'),
    'email' => $this->input->post('email'),
                'no_telp' => $this->input->post('no_telp'),
                'password' => $enc_password,
   );

   // Insert user
   return $this->db->insert('user', $data);
  }

  // Log user in
  

  // Check username exists
  public function check_no_telp_exists($no_telp){
   $query = $this->db->get_where('user', array('no_telp' => $no_telp));
   if(empty($query->row_array())){
    return true;
   } else {
    return false;
   }
  }

  // Check email exists
  public function check_email_exists($email){
   $query = $this->db->get_where('user', array('email' => $email));
   if(empty($query->row_array())){
    return true;
   } else {
    return false;
   }
  }
 }