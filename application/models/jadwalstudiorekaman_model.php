<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class jadwalstudiorekaman_model extends CI_Model{
    function __construct() {
        parent::__construct();
    }

    function get_all() {
        $this->db->join('studiorekaman','studiorekaman.id_studiorekaman = jadwalstudiorekaman.id_studiorekaman');   
        $this->db->join('sesistudiorekaman','sesistudiorekaman.id_sesistudiorekaman = jadwalstudiorekaman.id_sesistudiorekamanjoin');
        $this->db->join('user','user.id_user = jadwalstudiorekaman.id_user');
        return $this->db->get('jadwalstudiorekaman')->result(); 

    }

    function eventlist() {
        $this->db->join('studiorekaman','studiorekaman.id_studiorekaman = peminjamanstudiorekaman.id_studiorekaman');   
        $this->db->join('sesistudiorekaman','sesistudiorekaman.id_sesistudiorekaman = peminjamanstudiorekaman.id_sesistudiorekaman');
        $this->db->join('user','user.id_user = peminjamanstudiorekaman.id_user');
        return $this->db->get('peminjamanstudiorekaman'); 

    }
    function cek($tanggal,$id_studiorekaman,$id_sesistudiorekaman) {
        $this->db->join('studiorekaman','studiorekaman.id_studiorekaman = peminjamanstudiorekaman.id_studiorekaman');   
        $this->db->join('sesistudiorekaman','sesistudiorekaman.id_sesistudiorekaman = peminjamanstudiorekaman.id_sesistudiorekaman');
        $this->db->join('user','user.id_user = peminjamanstudiorekaman.id_user');
        $this->db->like('peminjamanstudiorekaman.tanggal',$tanggal);
        $this->db->where('peminjamanstudiorekaman.id_studiorekaman',$id_studiorekaman);
        $this->db->where('peminjamanstudiorekaman.id_sesistudiorekaman',$id_sesistudiorekaman);
        $this->db->where('peminjamanstudiorekaman.status !=',"Sewa Dibatalkan");

        return $this->db->get('peminjamanstudiorekaman')->row(); 

    }
     function create($data) {
        $this->db->insert('peminjamanstudiorekaman', $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;

    }

    function create_t($data2) {
        $this->db->insert('transaksistudiorekaman', $data2);
        return $this->db->get('transaksistudiorekaman')->row();

    }


    function get_by($id_jadwalstudiorekaman) {
        $this->db->where('id_jadwalstudiorekaman', $id_jadwalstudiorekaman);
        return $this->db->get('jadwalstudiorekaman')->row();    
    }

    function harga($id_studiorekaman) {
        $this->db->where('id_studiorekaman', $id_studiorekaman);
        return $this->db->get('studiorekaman')->row()->harga;    
    }

    function jammulai($id_sesistudiorekaman) {
        $this->db->where('id_sesistudiorekaman', $id_sesistudiorekaman);
        return $this->db->get('sesistudiorekaman')->row()->jammulai;    
    }

    function studiorekaman() {
        return $this->db->get('studiorekaman')->result();
    }

    function sesistudiorekaman() {
        return $this->db->get('sesistudiorekaman')->result();
    }

}
?>