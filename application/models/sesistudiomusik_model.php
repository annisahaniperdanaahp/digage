<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class sesistudiomusik_model extends CI_Model{
    function __construct() {
        parent::__construct();
    }

    function get_all() {
        return $this->db->get('sesistudiomusik')->result();    
    }

    function get_by($id_sesistudiomusik) {
        $this->db->where('id_sesistudiomusik', $id_sesistudiomusik);
        return $this->db->get('sesistudiomusik')->row();    
    }

    function create($data) {
        $this->db->insert('sesistudiomusik', $data);
        return $this->db->get('sesistudiomusik')->row();
    }

    function update($id_sesistudiomusik, $data) {
        $this->db->where('id_sesistudiomusik', $id_sesistudiomusik);
        $this->db->update('sesistudiomusik', $data);
    }

    function delete($id_sesistudiomusik) {
        $this->db->where('id_sesistudiomusik', $id_sesistudiomusik);
        $this->db->delete('sesistudiomusik');
    }
    
}
?>