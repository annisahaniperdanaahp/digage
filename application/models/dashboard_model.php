<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class dashboard_model extends CI_Model{
    function __construct() {
        parent::__construct();
    }
  
    function transaksi($hari){
        $this->db->join('peminjamanstudiomusik','peminjamanstudiomusik.id_peminjamanstudiomusik=transaksistudiomusik.id_peminjamanstudiomusik');
        $this->db->join('studiomusik','studiomusik.id_studiomusik=peminjamanstudiomusik.id_studiomusik');
        $this->db->join('sesistudiomusik','sesistudiomusik.id_sesistudiomusik=peminjamanstudiomusik.id_sesistudiomusik');
        $this->db->join('user','user.id_user=peminjamanstudiomusik.id_user');
        $this->db->like('peminjamanstudiomusik.tanggal', $hari);
        $this->db->where('peminjamanstudiomusik.status !=',"Sewa Dibatalkan");
        return $this->db->get('transaksistudiomusik')->result();    
    }
    function rekaman($hari){
        $this->db->join('peminjamanstudiorekaman','peminjamanstudiorekaman.id_peminjamanstudiorekaman=transaksistudiorekaman.id_peminjamanstudiorekaman');
        $this->db->join('studiorekaman','studiorekaman.id_studiorekaman=peminjamanstudiorekaman.id_studiorekaman');
        $this->db->join('sesistudiorekaman','sesistudiorekaman.id_sesistudiorekaman=peminjamanstudiorekaman.id_sesistudiorekaman');
        $this->db->join('user','user.id_user=peminjamanstudiorekaman.id_user');
        $this->db->like('peminjamanstudiorekaman.tanggal', $hari);
        $this->db->where('peminjamanstudiorekaman.status !=',"Sewa Dibatalkan");
        return $this->db->get('transaksistudiorekaman')->result();  
    }
     function studiomusik() {
        return $this->db->get('studiomusik')->result();
    }

    function sesistudiomusik() {
        return $this->db->get('sesistudiomusik')->result();    
    }
     function studiorekaman() {
        return $this->db->get('studiorekaman')->result();
    }

    function sesistudiorekaman() {
        return $this->db->get('sesistudiorekaman')->result();
    }
}