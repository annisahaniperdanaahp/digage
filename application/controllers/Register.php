<?php
defined('BASEPATH') OR exit('No direct script access allowed');
    class Register extends CI_Controller{
    function __construct(){
    parent:: __construct();    
    $this->load->model('user_model');
    $this->load->helper('url','form');
    $this->load->library(array('form_validation','session'));
    }
        // Register user
        public function index(){
            $data['title'] = 'Sign Up';

            $this->form_validation->set_rules('nama', 'Nama', 'required');
            $this->form_validation->set_rules('no_telp', 'No Telp', 'required|callback_check_no_telp_exists');
           $this->form_validation->set_rules('email', 'Email', 'required|callback_check_email_exists|callback_check_isGmail');
            $this->form_validation->set_rules('password', 'Password', 'required');
            $this->form_validation->set_rules('password2', 'Confirm Password', 'matches[password]');
             $this->form_validation->set_message('matches', 'Password Confirmation Salah');
            if($this->form_validation->run() === FALSE){
                $data = array(
                        'nama' => set_value('nama'),
                        'no_telp' => set_value('no_telp'),
                      'email' => set_value('email'),
                  );
                $this->load->view('template/register', $data);
            } else {
                // Encrypt password
                $enc_password = sha1($this->input->post('password'));

                $this->user_model->register($enc_password);

                $this->session->set_flashdata('user_registered', 'You are now registered and can log in');

                redirect('login_user');
            }
        }
        function check_isGmail($email) {
            $email = trim($email); 
            $this->form_validation->set_message('check_isGmail', 'Email harus menggunakan @gmail.com');
            if (mb_substr($email, -10) === '@gmail.com'){
                return true;
            }else{
                return false;
            }
        }
         public function check_no_telp_exists($no_telp){
            $this->form_validation->set_message('check_no_telp_exists', 'Nomor Telp Sudah diambil. Silahkan gunakan yang lain');
            if($this->user_model->check_no_telp_exists($no_telp)){
                return true;
            } else {
                return false;
            }
        }
        // Check if email exists
        public function check_email_exists($email){
            $this->form_validation->set_message('check_email_exists', 'Email Sudah diambil. Silahkan gunakan email lain');
            if($this->user_model->check_email_exists($email)){
                return true;
            } else {
                return false;
            }
        }
    }
