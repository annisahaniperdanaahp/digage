<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class login_user extends CI_Controller {
    
    function __construct(){
        parent::__construct();

        $this->load->model('login_model');
    }
    
    // Index login
    public function index() {
        // Fungsi Login
        $email = $this->session->userdata('email');
        // $role = $this->session->userdata('role');

        if($email !== NULL) {
           redirect(site_url('dashboard'));
        } else {
            $this->template->login();

        }

    }

    public function login_action() {
        $email = $this->input->post('email');
        $password = sha1($this->input->post('password'));

        $login    = $this->login->login($email, $password);
        
        if ($login==TRUE) {
            $this->session->set_flashdata('alert', 'sweetAlert("success", "Anda Berhasil Login");');
            redirect(site_url('dashboard'));
        } else{
            
                $this->load->view('template/login', $data);
        }

    }
    
    // Logout di sini
    public function logout() {
        $this->session->set_flashdata('alert', 'sweetAlert("success", "Berhasil Logout");');
        $this->login->logout();

    }   
}