<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class jadwalstudiomusik extends CI_Controller {

	function __construct(){
		parent::__construct();
		// $this->login->cek_login();

		$this->load->model('jadwalstudiomusik_model');
		$this->load->model('studiomusik_model'); 
		// $this->load->model('sesi_model');
	}

	public function index() {
		$data = array(
			'title' 			=> 'Data Studio Musik Digage',
			'sidebar' 			=> 'jadwalstudiomusik',

			'jadwalstudiomusik' => $this->jadwalstudiomusik_model->get_all(),
			'kalender'			=> $this->jadwalstudiomusik_model->eventlist(),
			'studiom'				=> $this->jadwalstudiomusik_model->studiomusik(),
			'sesistudiom'			=> $this->jadwalstudiomusik_model->get_sesi(),
		);

		$this->template->display('studiomusik/jadwalstudiomusik_list', $data);
	}
	public function cek(){
		$tanggal= $this->input->post('tanggal');
		$id_sesistudiomusik= $this->input->post('id_sesistudiomusik');
		$id_studiomusik= $this->input->post('id_studiomusik');
		$data=$this->jadwalstudiomusik_model->cek($tanggal,$id_studiomusik,$id_sesistudiomusik);
		echo json_encode($data);
	}
	public function dashboard() {
		$data = array( 
			'sidebar' => 'dashboard',
		);

		$this->template->display('dashboard', $data);
	}

public function create() {
		$data = array(
				'title' 	    => 'Tambah',
				'sidebar' 	    => 'jadwalstudiomusik',

				'action' 	    => site_url('jadwalstudiomusik/create_action'),
				'id_jadwalstudiomusik'			=> set_value('id_jadwalstudiomusik'),
				'id_studiomusik' 				=> set_value('id_studiomusik'),
				'id_sesistudiomusik'			=> set_value('id_sesistudiomusik'),
				'tanggal'						=> set_value('tanggal'),
				'status'						=> set_value('status'),



			);

		$this->template->display('jadwalstudiomusik/jadwalstudiomusik_form', $data);
	}

	public function create_action() {
		$id_studiomusik = $this->input->post('id_studiomusik');
		$id_sesistudiomusik = $this->input->post('id_sesistudiomusik');
		// $dataab['waktu'] = date('d-m-Y H:i', strtotime($this->input->post('tanggal')));
		// $dataab['jammulai'] = date('H:i', strtotime($this->jadwalstudiomusik_model->jammulai($id_sesistudiomusik))) ;
		$waktu = strtotime($this->input->post('tanggal'));
		$jammulai = (substr($this->jadwalstudiomusik_model->jammulai($id_sesistudiomusik), -5,2) * 60 *60) ;
		$total = date('Y-m-d H:i', ($waktu+$jammulai));
		// $jammulai
		// $dataab['jammulai1'] = strtotime($dataab['jammulai']);
		// $dataa['waktu1'] = strtotime($this->input->post('tanggal'));
		// $dataa['jammulai1'] = strtotime($this->jadwalstudiomusik_model->jammulai($id_sesistudiomusik));
		// $jammulai1 = strtotime($jammulai);
		$data = array(
				'id_studiomusik' 				=> $this->input->post('id_studiomusik'),
				'id_sesistudiomusik' 			=> $this->input->post('id_sesistudiomusik'),
				'id_user'						=> $this->session->userdata('id_user'),
				'tanggal'						=> $total,
				'status'						=> 'Disewa', 

			);

		$data3 = $this->jadwalstudiomusik_model->create($data);
		$data2 = array(
				'id_peminjamanstudiomusik' 		=> $data3,
				'total_harga'					=> $this->jadwalstudiomusik_model->harga($id_studiomusik),
				'keterangan'					=> 'Belum Bayar',

			);
		$this->jadwalstudiomusik_model->create_t($data2);
		
		redirect(site_url('jadwalstudiomusik'));
	}

}