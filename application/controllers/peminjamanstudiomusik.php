<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class peminjamanstudiomusik extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->login->cek_login();

		$this->load->model('peminjamanstudiomusik_model');
	}

	public function index() {
		$id_user = $this->session->userdata('id_user');
		$data = array(
			'title' 			=> 'Data Studio Musik Digage',
			'sidebar' 			=> 'peminjamanstudiomusik',

			'action'			=> site_url('peminjamanstudiomusik/create_action'),
			'action1'			=> site_url('peminjamanstudiomusik/update_action'),
			'studiom'				=> $this->peminjamanstudiomusik_model->studiomusik(),
			'sesistudiom'			=> $this->peminjamanstudiomusik_model->sesistudiomusik(),
			'peminjamanstudiomusik' => $this->peminjamanstudiomusik_model->get_all_transaksi($id_user),
		);


		$this->template->display('studiomusik/peminjamanstudiomusik_list', $data);
	}

	public function dashboard() {
		$data = array(
			'sidebar' => 'dashboard',
		);

		$this->template->display('dashboard', $data);
	}

	// Manajemen Data
	public function create() {
		$data = array(
				'title' 	    => 'Tambah',
				'sidebar' 	    => 'peminjamanstudiomusik',

				'action' 	    => site_url('peminjamanstudiomusik/create_action'),
				'id_peminjamanstudiomusik'		=> set_value('id_peminjamanstudiomusik'),
				'id_studiomusik' 				=> set_value('id_studiomusik'),
				'id_sesistudiomusik'			=> set_value('id_sesistudiomusik'),
				'harga'							=> set_value('harga'),
				'tanggal'						=> set_value('tanggal'),



			);

		$this->template->display('peminjamanstudiomusik/peminjamanstudiomusik_form', $data);
	}
 
	public function create_action() {
		$id_sesistudiomusik = $this->input->post('id_sesistudiomusik');
		$jammulai = $this->peminjamanstudiomusik_model->jammulai($id_sesistudiomusik);
		$jammulai1 = strtotime($jammulai);
		$data = array(
				'id_studiomusik' 				=> $this->input->post('id_studiomusik'),
				'id_sesistudiomusik' 			=> $this->input->post('id_sesistudiomusik'),
				'id_user'						=> $this->session->userdata('id_user'),
				'tanggal'						=> $this->input->post('tanggal'),

			);
		echo "<pre>";
		print_r($data);
		print_r($jammulai1);
		// print_r($data2);
		echo "<pre>";
		exit();
		$data3 = $this->peminjamanstudiomusik_model->create($data);
		$data2 = array(
				'id_peminjamanstudiomusik' 		=> $data3,
				'harga_total'					=> $this->peminjamanstudiomusik_model->harga($id_sesistudiomusik),
				'keterangan'					=> 'Belum Bayar',
		);
		$this->peminjamanstudiomusik_model->create_t($data2);

		redirect(site_url('peminjamanstudiomusik'));
	}

	public function update($id_peminjamanstudiomusik) {
		$peminjamanstudiomusik = $this->peminjamanstudiomusik_model->get_by($id_peminjamanstudiomusik);

		$data = array(
				'title' 	=> 'Edit',
				'sidebar' 	=> 'peminjamanstudiomusik',

				'action' 		=> site_url('peminjamanstudiomusik/update_action'),
				'id_peminjamanstudiomusik'=> set_value('id_peminjamanstudiomusik', $peminjamanstudiomusik->id_peminjamanstudiomusik),
				'peminjamanstudiomusik' 	=> set_value('nama_studio', $peminjamanstudiomusik->nama_studio),

			);

		$this->template->display('peminjamanstudiomusik/peminjamanstudiomusik_form', $data);
	} 

	public function update_action() {
		$id_peminjamanstudiomusik = $this->input->post('id_peminjamanstudiomusik');

		$data = array(
				'nama_studio' 	=> $this->input->post('nama_studio'),
		);

		$this->peminjamanstudiomusik_model->update($id_peminjamanstudiomusik, $data);
		redirect(site_url('peminjamanstudiomusik'));
	}

	public function cancel($id_peminjamanstudiomusik) {
		
		$data = array(
                    'status' 	=> 'Menunggu Konfirmasi Pembatalan',
		);
		$this->peminjamanstudiomusik_model->cancel($id_peminjamanstudiomusik, $data);
		redirect(site_url('peminjamanstudiomusik'));
	}

	public function delete($id_peminjamanstudiomusik) {
		$this->peminjamanstudiomusik_model->delete($id_peminjamanstudiomusik);

		redirect(site_url('peminjamanstudiomusik'));
	}

	public function pdf($id){
		$this->load->library('dompdf_gen');

		$id_user = $this->session->userdata('id_user');

		$query = $this->peminjamanstudiomusik_model->cetak($id,$id_user.'transaksistudiomusik');
		foreach ($query as $key => $value) {
			$namapemesan = $value->nama;
			$nama_studio = $value->nama_studio;
			$tanggal = $value->tanggal;
			$namasesi = $value->namasesi;
			$jammulai = $value->jammulai;
			$jamakhir = $value->jamakhir;
			$fasilitas = $value->fasilitas;
			$total_harga = $value->total_harga;
			$keterangan = $value->keterangan;
		}
		$data = array(
			'namapemesan'=>$namapemesan,
			'nama_studio'=>$nama_studio,
			'tanggal'=>$tanggal,
			'namasesi'=>$namasesi,
			'jammulai'=>$jammulai,
			'jamakhir'=>$jamakhir,
			'fasilitas'=>$fasilitas,
			'total_harga'=>$total_harga,
			'keterangan'=>$keterangan,
			'peminjamanstudiomusik'=>$query
		);
		$this->load->view('studiomusik/buktisewa', $data);

		$paper_size = 'A5';
		$orientation = 'potrait';
		$html = $this->output->get_output();
		$this->dompdf->set_paper($paper_size, $orientation);

		$this->dompdf->load_html($html);
		$this->dompdf->render();
		$this->dompdf->stream("buktisewa.pdf", array('Attachment' =>0)
		);
	}
}