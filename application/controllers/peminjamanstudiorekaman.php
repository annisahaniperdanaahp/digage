<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class peminjamanstudiorekaman extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->login->cek_login();

		$this->load->model('peminjamanstudiorekaman_model');
	}

	public function index() {
		$id_user = $this->session->userdata('id_user');
		$data = array(
			'title' 			=> 'Data Studio rekaman Digage',
			'sidebar' 			=> 'peminjamanstudiorekaman',

			'action'			=> site_url('peminjamanstudiorekaman/create_action'),
			'action1'			=> site_url('peminjamanstudiorekaman/update_action'),
			'studiom'				=> $this->peminjamanstudiorekaman_model->studiorekaman(),
			'sesistudiom'			=> $this->peminjamanstudiorekaman_model->sesistudiorekaman(),
			'peminjamanstudiorekaman' => $this->peminjamanstudiorekaman_model->get_all($id_user),
		);


		$this->template->display('studiorekaman/peminjamanstudiorekaman_list', $data);
	}

	public function dashboard() {
		$data = array(
			'sidebar' => 'dashboard',
		);

		$this->template->display('dashboard', $data);
	}

	// Manajemen Data
	public function create() {
		$data = array(
				'title' 	    => 'Tambah',
				'sidebar' 	    => 'peminjamanstudiorekaman',

				'action' 	    => site_url('peminjamanstudiorekaman/create_action'),
				'id_peminjamanstudiorekaman'		=> set_value('id_peminjamanstudiorekaman'),
				'id_studiorekaman' 				=> set_value('id_studiorekaman'),
				'id_sesistudiorekaman'			=> set_value('id_sesistudiorekaman'),
				'harga'							=> set_value('harga'),
				'tanggal'						=> set_value('tanggal'),



			);

		$this->template->display('peminjamanstudiorekaman/peminjamanstudiorekaman_form', $data);
	}
 
	public function create_action() {
		$id_sesistudiorekaman = $this->input->post('id_sesistudiorekaman');
		$jammulai = $this->peminjamanstudiorekaman_model->jammulai($id_sesistudiorekaman);
		$jammulai1 = strtotime($jammulai);
		$data = array(
				'id_studiorekaman' 				=> $this->input->post('id_studiorekaman'),
				'id_sesistudiorekaman' 			=> $this->input->post('id_sesistudiorekaman'),
				'id_user'						=> $this->session->userdata('id_user'),
				'tanggal'						=> $this->input->post('tanggal'),

			);
		echo "<pre>";
		print_r($data);
		print_r($jammulai1);
		// print_r($data2);
		echo "<pre>";
		exit();
		$data3 = $this->peminjamanstudiorekaman_model->create($data);
		$data2 = array(
				'id_peminjamanstudiorekaman' 		=> $data3,
				'harga_total'					=> $this->peminjamanstudiorekaman_model->harga($id_sesistudiorekaman),
				'keterangan'					=> 'Belum Bayar',
		);
		$this->peminjamanstudiorekaman_model->create_t($data2);

		
		redirect(site_url('peminjamanstudiorekaman'));
	}

	public function update($id_peminjamanstudiorekaman) {
		$peminjamanstudiorekaman = $this->peminjamanstudiorekaman_model->get_by($id_peminjamanstudiorekaman);

		$data = array(
				'title' 	=> 'Edit',
				'sidebar' 	=> 'peminjamanstudiorekaman',

				'action' 		=> site_url('peminjamanstudiorekaman/update_action'),
				'id_peminjamanstudiorekaman'=> set_value('id_peminjamanstudiorekaman', $peminjamanstudiorekaman->id_peminjamanstudiorekaman),
				'peminjamanstudiorekaman' 	=> set_value('nama_studio', $peminjamanstudiorekaman->nama_studio),

			);

		$this->template->display('peminjamanstudiorekaman/peminjamanstudiorekaman_form', $data);
	} 

	public function update_action() {
		$id_peminjamanstudiorekaman = $this->input->post('id_peminjamanstudiorekaman');

		$data = array(
				'nama_studio' 	=> $this->input->post('nama_studio'),
		);

		$this->peminjamanstudiorekaman_model->update($id_peminjamanstudiorekaman, $data);
		redirect(site_url('peminjamanstudiorekaman'));
	}

	public function cancel($id_peminjamanstudiorekaman) {
		// $id_peminjamanstudiorekaman = $this->input->post('id_peminjamanstudiorekaman');

		$data = array(
				'status' 	=> 'Menunggu Konfirmasi Pembatalan',
		);

		$this->peminjamanstudiorekaman_model->cancel($id_peminjamanstudiorekaman, $data);
		redirect(site_url('peminjamanstudiorekaman'));
	}

	public function delete($id_peminjamanstudiorekaman) {
		$this->peminjamanstudiorekaman_model->delete($id_peminjamanstudiorekaman);

		redirect(site_url('peminjamanstudiorekaman'));
	}
	public function pdf($id){

		$this->load->library('dompdf_gen');

		$id_user = $this->session->userdata('id_user');

		$query = $this->peminjamanstudiorekaman_model->cetak($id,$id_user.'transaksistudiomusik');
		
		foreach ($query as $key => $value) {
			$namapemesan = $value->nama;
			$nama_studio = $value->nama_studio;
			$tanggal = $value->tanggal;
			$namasesi = $value->namasesi;
			$jammulai = $value->jammulai;
			$jamakhir = $value->jamakhir;
			$fasilitas = $value->fasilitas;
			$total_harga = $value->total_harga;
			$keterangan = $value->keterangan;
		}
		$data = array(
			'namapemesan'=>$namapemesan,
			'nama_studio'=>$nama_studio,
			'tanggal'=>$tanggal,
			'namasesi'=>$namasesi,
			'jammulai'=>$jammulai,
			'jamakhir'=>$jamakhir,
			'fasilitas'=>$fasilitas,
			'total_harga'=>$total_harga,
			'keterangan'=>$keterangan,
			'peminjamanstudiomusik'=>$query
		);
		$this->load->view('studiorekaman/buktisewa', $data);

		$paper_size = 'A5';
		$orientation = 'potrait';
		$html = $this->output->get_output();
		$this->dompdf->set_paper($paper_size, $orientation);

		$this->dompdf->load_html($html);
		$this->dompdf->render();
		$this->dompdf->stream("buktisewa.pdf", array('Attachment' =>0)
		);
	}
}