<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class jadwalstudiorekaman extends CI_Controller {

	function __construct(){
		parent::__construct();

		$this->load->model('jadwalstudiorekaman_model');
		$this->load->model('studiorekaman_model'); 

	}

	public function index() {
		$data = array(
			'title' 			=> 'Data Studio rekaman Digage',
			'sidebar' 			=> 'jadwalstudiorekaman',

			'jadwalstudiorekaman' => $this->jadwalstudiorekaman_model->get_all(),
			'kalender'			=> $this->jadwalstudiorekaman_model->eventlist(),
			'studiom'				=> $this->jadwalstudiorekaman_model->studiorekaman(),
			'sesistudiom'			=> $this->jadwalstudiorekaman_model->sesistudiorekaman(),
		);

		$this->template->display('studiorekaman/jadwalstudiorekaman_list', $data);
	}
	public function cek(){
		$tanggal= $this->input->post('tanggal');
		$id_sesistudiorekaman= $this->input->post('id_sesistudiorekaman');
		$id_studiorekaman= $this->input->post('id_studiorekaman');
		$data=$this->jadwalstudiorekaman_model->cek($tanggal,$id_studiorekaman,$id_sesistudiorekaman);
		echo json_encode($data);
	}
	public function dashboard() {
		$data = array( 
			'sidebar' => 'dashboard',
		);

		$this->template->display('dashboard', $data);
	}

public function create() {
		$data = array(
				'title' 	    => 'Tambah',
				'sidebar' 	    => 'jadwalstudiorekaman',

				'action' 	    => site_url('jadwalstudiorekaman/create_action'),
				'id_jadwalstudiorekaman'			=> set_value('id_jadwalstudiorekaman'),
				'id_studiorekaman' 				=> set_value('id_studiorekaman'),
				'id_sesistudiorekaman'			=> set_value('id_sesistudiorekaman'),
				'tanggal'						=> set_value('tanggal'),
				'status'						=> set_value('status'),



			);

		$this->template->display('jadwalstudiorekaman/jadwalstudiorekaman_form', $data);
	}

	public function create_action() {
		$id_studiorekaman = $this->input->post('id_studiorekaman');
		$id_sesistudiorekaman = $this->input->post('id_sesistudiorekaman');
		
		$waktu = strtotime($this->input->post('tanggal'));
		$jammulai = (substr($this->jadwalstudiorekaman_model->jammulai($id_sesistudiorekaman), -5,2) * 60 *60) ;
		$total = date('Y-m-d H:i', ($waktu+$jammulai));
		$data = array(
				'id_studiorekaman' 				=> $this->input->post('id_studiorekaman'),
				'id_sesistudiorekaman' 			=> $this->input->post('id_sesistudiorekaman'),
				'id_user'						=> $this->session->userdata('id_user'),
				'tanggal'						=> $total,
				'status'						=> 'Disewa', 

			);
		
		$data3 = $this->jadwalstudiorekaman_model->create($data);
		$data2 = array(
				'id_peminjamanstudiorekaman' 		=> $data3,
				'total_harga'					=> $this->jadwalstudiorekaman_model->harga($id_studiorekaman),
				'keterangan'					=> 'Belum Bayar',

			);
		$this->jadwalstudiorekaman_model->create_t($data2);
		redirect(site_url('jadwalstudiorekaman'));
	}

	
}