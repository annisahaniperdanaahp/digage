<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class dashboard extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->login->cek_login();

        // $this->load->model('alatmusik_model');
        // $this->load->model('studimusik_model');
         $this->load->model('dashboard_model');


    }


    public function index() {
        $studiomusik = array();
        $hari = date('Y-m-d', time());
        $hari1 = date('Y-m-d', strtotime('+1 days', strtotime($hari)));
        $hari2 = date('Y-m-d', strtotime('+2 days', strtotime($hari)));       
        
        $haria = $this->dashboard_model->transaksi($hari);
        if(count($haria)!=0){
            $h1 = array();
            foreach ($haria as $key => $value) {
                    $ar = array(
                        'ruang'=>$value->nama_studio,
                        'sesi'=>$value->namasesi,
                        'jammulai'=>$value->jammulai,
                        'jamakhir'=>$value->jamakhir,
                    );
                    array_push($h1, $ar);
            }
            $har1 ['tanggal'] = $hari;
            $har1 ['data'] = $h1;
             array_push($studiomusik, $har1);
        }else{
            $har1 ['tanggal'] = $hari;
            $har1 ['data'] = array();
             array_push($studiomusik, $har1);
        }
        $harib = $this->dashboard_model->transaksi($hari1);
         if(count($harib)!=0){
            $h2 = array();
            foreach ($harib as $key => $value) {
                    $ar = array(
                        'ruang'=>$value->nama_studio,
                        'sesi'=>$value->namasesi,
                        'jammulai'=>$value->jammulai,
                        'jamakhir'=>$value->jamakhir,
                    );
                    array_push($h2, $ar);
            }
            $har2 ['tanggal'] = $hari1;
            $har2 ['data'] = $h2;
             array_push($studiomusik, $har2);
        }else{
             $har2 ['tanggal'] = $hari1;
            $har2 ['data'] = array();
             array_push($studiomusik, $har2);
        }
        $haric = $this->dashboard_model->transaksi($hari2);
         if(count($haric)!=0){
            $h3 = array();
            foreach ($haric as $key => $value) {
                    $ar = array(
                        'ruang'=>$value->nama_studio,
                        'sesi'=>$value->namasesi,
                        'jammulai'=>$value->jammulai,
                        'jamakhir'=>$value->jamakhir,
                    );
                    array_push($h3, $ar);
            }
            $har3 ['tanggal'] = $hari2;
            $har3 ['data'] = $h3;
            array_push($studiomusik, $har3);
        }else{
              $har3 ['tanggal'] = $hari2;
            $har3 ['data'] = array();
            array_push($studiomusik, $har3);
        }
        $studiorekaman = array();
        $harid = $this->dashboard_model->rekaman($hari);
        if(count($haria)!=0){
            $h4 = array();
            foreach ($harid as $key => $value) {
                    $ar = array(
                        'ruang'=>$value->nama_studio,
                        'sesi'=>$value->namasesi,
                        'jammulai'=>$value->jammulai,
                        'jamakhir'=>$value->jamakhir,
                    );
                    array_push($h4, $ar);
            }
            $har4 ['tanggal'] = $hari;
            $har4 ['data'] = $h4;
             array_push($studiorekaman, $har4);
        }else{
            $har4 ['tanggal'] = $hari;
            $har4 ['data'] = array();
            array_push($studiorekaman, $har4);
        }
        $harie = $this->dashboard_model->rekaman($hari1);
         if(count($harie)!=0){
            $h5 = array();
            foreach ($harie as $key => $value) {
                    $ar = array(
                        'ruang'=>$value->nama_studio,
                        'sesi'=>$value->namasesi,
                        'jammulai'=>$value->jammulai,
                        'jamakhir'=>$value->jamakhir,
                    );
                    array_push($h5, $ar);
            }
            $har5 ['tanggal'] = $hari1;
            $har5 ['data'] = $h5;
             array_push($studiorekaman, $har5);
        }else{
            $har5 ['tanggal'] = $hari1;
            $har5 ['data'] = array();
             array_push($studiorekaman, $har5);
        }
        $harif = $this->dashboard_model->rekaman($hari2);
         if(count($harif)!=0){
            $h6 = array();
            foreach ($harif as $key => $value) {
                    $ar = array(
                        'ruang'=>$value->nama_studio,
                        'sesi'=>$value->namasesi,
                        'jammulai'=>$value->jammulai,
                        'jamakhir'=>$value->jamakhir,
                    );
                    array_push($h6, $ar);
            }
            $har6 ['tanggal'] = $hari2;
            $har6 ['data'] = $h6;
            array_push($studiorekaman, $har6);
        }else{
             $har6 ['tanggal'] = $hari2;
            $har6 ['data'] = array();
            array_push($studiorekaman, $har6);
        }
       $data = array(
            'title'             => 'Data Alat Musik Digage',
            'sidebar'           => 'dashboard',
            'studiomusik'       => $studiomusik,
            'studiorekaman'       => $studiorekaman
            
        );
        $this->template->display('dashboard', $data);

    }
}