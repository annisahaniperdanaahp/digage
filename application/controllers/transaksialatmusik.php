<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class transaksialatmusik extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->login->cek_login();

        $this->load->model('transaksialatmusik_model');
        $this->load->model('alatmusik_model');
    }

    public function index() {
        $data = array(
            'title'             => 'Data Alat Musik Digage',
            'sidebar'           => 'transaksialatmusik',

            'transaksialatmusik'         => $this->transaksialatmusik_model->get_all(),
        );

        $this->template->display('alatmusik/transaksialatmusik_list', $data);
    }

    public function dashboard() {
        $data = array(
            'sidebar' => 'dashboard',
        );

        $this->template->display('dashboard', $data);
    }

    // Manajemen Data
    public function create() {

        $data = array(
                'title'         => 'Tambah',
                'sidebar'       => 'transaksialatmusik',

                'action'        => site_url('transaksialatmusik/create_action'),
                'id_transaksialatmusik'    => set_value('id_transaksialatmusik'),
                'id_alatmusik'  => set_value('id_alatmusik'),
                'id_user'       => set_value('id_user'),
                'jumlah'        => set_value('jumlah'),
                'tgl_pinjam'    => set_value('tgl_pinjam'),
                'tgl_kembali'   => set_value('tgl_kembali'),
                'ktp'           => set_value('ktp'),
                'total_harga'   => set_value('total_harga'),
            );

        $this->template->display('alatmusik/transaksialatmusik_form', $data);
    }

    public function create_action() {

        $id_transaksialatmusik = $this->transaksialatmusik_model->upload_alat();

        $data = array(
                'id_transaksialatmusik'     => $id_transaksialatmusik,
                'id_alatmusik'     => $this->input->post('id_alatmusik'),
                'id_user'          => $this->input->post('id_user'),
                'jumlah'           => $this->input->post('jumlah'),
                'tgl_pinjam'       => $this->input->post('tgl_pinjam'),
                'tgl_kembali'      => $this->input->post('tgl_kembali'),
                'ktp'              => $this->input->post('ktp'),
                'total_harga'      => $this->input->post('total_harga'),

            );
    

        if(!is_dir('../uploads/transaksialatmusik/'.$id_transaksialatmusik)){
            mkdir('../uploads/transaksialatmusik/'.$id_transaksialatmusik, 0755, TRUE);
        }


        $config['upload_path']      = '../uploads/transaksialatmusik/'.$id_transaksialatmusik;
        $config['allowed_types']     = 'gif|jpg|png';

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('gambar')) {
            $this->session->set_flashdata('alert', 'sweetAlert("error", "Gagal", "Gambar tidak dapat di upload");');

            rmdir('../uploads/transaksialatmusik/'.$id_transaksialatmusik);
            redirect(site_url('transaksialatmusik/create'));
        }else{
            $scan = array('upload_data' => $this->upload->data());
            $data['gambar'] = $scan['upload_data']['file_name'];

            $this->transaksialatmusik_model->create($data);
            $this->session->set_flashdata('alert', 'sweetAlert("success", "Berhasil disimpan");');

            redirect(site_url('transaksialatmusik'));
        }
    }

    public function update($id_transaksialatmusik) {
        $transaksialatmusik = $this->transaksialatmusik_model->get_by($id_transaksialatmusik);

        $data = array(
                'title'     => 'Edit',
                'sidebar'   => 'transaksialatmusik',

                'action'      => site_url('transaksialatmusik/update_action'),
                'id_transaksialatmusik'=> set_value('id_transaksialatmusik', $transaksialatmusik->id_transaksialatmusik),
                'nama_alat'   => set_value('nama_alat', $transaksialatmusik->nama_alat),
                'hargasewa'   => set_value('hargasewa', $transaksialatmusik->hargasewa),
                'gambar'      => set_value('gambar', $transaksialatmusik->gambar),
                'deskripsi'   => set_value('deskripsi', $transaksialatmusik->deskripsi),
                'jumlah'      => set_value('jumlah', $transaksialatmusik->jumlah),

            );

        $this->template->display('transaksialatmusik/transaksialatmusik_form', $data);
    }

    public function update_action() {
        $id_transaksialatmusik = $this->input->post('id_transaksialatmusik');

        $data = array(
                'nama_alat'    => $this->input->post('nama_alat'),
                'hargasewa'    => $this->input->post('hargasewa'),
                'gambar'       => $this->input->post('gambar'),
                'deskripsi'    => $this->input->post('deskripsi'),
                'jumlah'       => $this->input->post('jumlah'),
        );


        $config['upload_path']      = '../uploads/transaksialatmusik/'.$id_transaksialatmusik;
        $config['allowed_types']     = 'gif|jpg|png';

        $this->load->library('upload', $config);

        if (!is_dir('../uploads/transaksialatmusik'.$id_transaksialatmusik)) {
            mkdir('../uploads/transaksialatmusik/'.$id_transaksialatmusik, 0755, TRUE);
        }

        $transaksialatmusik = $this->transaksialatmusik_model->get_by($id_transaksialatmusik);

         if (!$this->upload->do_upload('gambar')) {
            $this->session->set_flashdata('alert', 'sweetAlert("error", "Gagal", "Gambar tidak dapat di upload");');

            redirect(site_url('transaksialatmusik/update'.$id_transaksialatmusik));
        }else{
            unlink('../uploads/transaksialatmusik'.$id_transaksialatmusik.'/'.$transaksialatmusik->gambar); //hapus foto lama

            $scan = array('upload_data' => $this->upload->data());
            $data['gambar'] = $scan['upload_data']['file_name'];

            $this->transaksialatmusik_model->update($id_transaksialatmusik, $data);
            $this->session->set_flashdata('alert', 'sweetAlert("success", "Berhasil Disimpan");');

            redirect(site_url('transaksialatmusik'));
        }

    }

    public function delete($id_transaksialatmusik) {
        $this->transaksialatmusik_model->delete($id_transaksialatmusik);

        redirect(site_url('transaksialatmusik'));
    }


    
}