<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class jadwalstudiorekaman_model extends CI_Model{
    function __construct() {
        parent::__construct();
    }

    function get_all() {
        $this->db->join('studiorekaman','studiorekaman.id_studiorekaman = jadwalstudiorekaman.id_studiorekaman');   
        $this->db->join('sesistudiorekaman','sesistudiorekaman.id_sesistudiorekaman = jadwalstudiorekaman.id_sesistudiorekamanjoin');
        $this->db->join('user','user.id_user = jadwalstudiorekaman.id_user');
        return $this->db->get('jadwalstudiorekaman')->result(); 

    }

    function eventlist() {
        $this->db->join('studiorekaman','studiorekaman.id_studiorekaman = peminjamanstudiorekaman.id_studiorekaman');   
        $this->db->join('sesistudiorekaman','sesistudiorekaman.id_sesistudiorekaman = peminjamanstudiorekaman.id_sesistudiorekaman');
        $this->db->join('user','user.id_user = peminjamanstudiorekaman.id_user');
        return $this->db->get('peminjamanstudiorekaman'); 

    }

    function get_by($id_jadwalstudiorekaman) {
        $this->db->where('id_jadwalstudiorekaman', $id_jadwalstudiorekaman);
        return $this->db->get('jadwalstudiorekaman')->row();    
    }

    // function create($data) {
    //     $this->db->insert('jadwalstudiorekaman', $data);
    //     return $this->db->get('jadwalstudiorekaman')->row();
    // }

    // function update($id_jadwalstudiorekaman, $data) {
    //     $this->db->where('id_jadwalstudiorekaman', $id_jadwalstudiorekaman);
    //     $this->db->update('nama_ruang', $data);
    // }

    // function delete($id_jadwalstudiorekaman) {
    //     $this->db->where('id_jadwalstudiorekaman', $id_jadwalstudiorekaman);
    //     $this->db->delete('jadwalstudiorekaman');
    // }
    
}
?>