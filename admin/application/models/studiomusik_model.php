<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class studiomusik_model extends CI_Model{
    function __construct() {
        parent::__construct();
    }

    function get_all() {
        return $this->db->get('studiomusik')->result();    
    }

    function get_by($id_studiomusik) {
        $this->db->where('id_studiomusik', $id_studiomusik);
        return $this->db->get('studiomusik')->row();    
    }

    function create($data) {
        $this->db->insert('studiomusik', $data);
        return $this->db->get('studiomusik')->row();
    }

    function update($id_studiomusik, $data) {
        $this->db->where('id_studiomusik', $id_studiomusik);
        $this->db->update('studiomusik', $data);
    }

    function delete($id_studiomusik) {
        $this->db->where('id_studiomusik', $id_studiomusik);
        $this->db->delete('studiomusik');
    }
    
}
?>