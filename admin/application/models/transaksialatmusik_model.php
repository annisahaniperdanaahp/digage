<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class transaksialatmusik_model extends CI_Model{
    function __construct() {
        parent::__construct();
    }

    function get_all() {
        $this->db->join('user','user.id_user = transaksialatmusik.id_user');
        $this->db->join('alatmusik','alatmusik.id_alatmusik = transaksialatmusik.id_alatmusik');
        
        return $this->db->get('transaksialatmusik')->result();    
    }

    function get_by($id_transaksialatmusik) {
        $this->db->where('id_transaksialatmusik', $id_transaksialatmusik);
        return $this->db->get('transaksialatmusik')->row();    
    }

    function create($data) {
        $this->db->insert('transaksialatmusik', $data);
        // return $this->db->get('transaksialatmusik')->row();
    }

    // function update($id_transaksialatmusik, $data) {
    //     $this->db->where('id_transaksialatmusik', $id_transaksialatmusik);
    //     $this->db->update('nama_alat', $data);
    // }

    function delete($id_transaksialatmusik) {
        $this->db->where('id_transaksialatmusik', $id_transaksialatmusik);
        $this->db->delete('transaksialatmusik');
    }

    function upload_alat(){
        $this->db->select('AUTO_INCREMENT');
        $this->db->where('TABLE_SCHEMA', $this->db->database);
        $this->db->where('TABLE_NAME','transaksialatmusik');
        $query = $this->db->get('INFORMATION_SCHEMA.TABLES')->row();

        return $query->AUTO_INCREMENT;

    }
}
?>