<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class jadwalstudiomusik_model extends CI_Model{
    function __construct() {
        parent::__construct();
    }

    function get_all() {
        $this->db->join('studiomusik','studiomusik.id_studiomusik = jadwalstudiomusik.id_studiomusik');   
        $this->db->join('sesistudiomusik','sesistudiomusik.id_sesistudiomusik = jadwalstudiomusik.id_sesistudiomusikjoin');
        $this->db->join('user','user.id_user = jadwalstudiomusik.id_user');
        return $this->db->get('jadwalstudiomusik')->result(); 

    }

    function eventlist() {
        $this->db->join('studiomusik','studiomusik.id_studiomusik = peminjamanstudiomusik.id_studiomusik');   
        $this->db->join('sesistudiomusik','sesistudiomusik.id_sesistudiomusik = peminjamanstudiomusik.id_sesistudiomusik');
        $this->db->join('user','user.id_user = peminjamanstudiomusik.id_user');
        return $this->db->get('peminjamanstudiomusik'); 

    }

    function get_by($id_jadwalstudiomusik) {
        $this->db->where('id_jadwalstudiomusik', $id_jadwalstudiomusik);
        return $this->db->get('jadwalstudiomusik')->row();    
    }

}
?>