<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class peminjamanstudiorekaman_model extends CI_Model{
    function __construct() {
        parent::__construct();
    }

    function get_all() {

        $this->db->join('studiorekaman','studiorekaman.id_studiorekaman=peminjamanstudiorekaman.id_studiorekaman');
        $this->db->join('sesistudiorekaman','sesistudiorekaman.id_sesistudiorekaman=peminjamanstudiorekaman.id_sesistudiorekaman');
        $this->db->join('user','user.id_user=peminjamanstudiorekaman.id_user');
        return $this->db->get('peminjamanstudiorekaman')->result();    
    }

    function get_by($id_peminjamanstudiorekaman) {
        $this->db->where('id_peminjamanstudiorekaman', $id_peminjamanstudiorekaman);
        return $this->db->get('peminjamanstudiorekaman')->row();    
    }

    function create($data) {
        $this->db->insert('peminjamanstudiorekaman', $data);
        return $this->db->get('peminjamanstudiorekaman')->row();

    }
    
     function confirm_transaksi($id, $data) {
        $this->db->where('id_transaksistudiorekaman', $id);
        $this->db->update('transaksistudiorekaman', $data);
    }
    function studiorekaman() {
        return $this->db->get('studiorekaman')->result();
    }

    function sesistudiorekaman() {
        return $this->db->get('sesistudiorekaman')->result();
    }

    function update($id_peminjamanstudiorekaman, $data) {
        $this->db->where('id_peminjamanstudiorekaman', $id_peminjamanstudiorekaman);
        $this->db->update('peminjamanstudiorekaman', $data);
    }

    function confirm($id_peminjamanstudiorekaman, $data) {
        $this->db->where('id_peminjamanstudiorekaman', $id_peminjamanstudiorekaman);
        $this->db->update('peminjamanstudiorekaman', $data);
    }

    function delete($id_peminjamanstudiorekaman) {
        $this->db->where('id_peminjamanstudiorekaman', $id_peminjamanstudiorekaman);
        $this->db->delete('peminjamanstudiorekaman');
    }
    function get_all_transaksi() {
          $this->db->join('peminjamanstudiorekaman','peminjamanstudiorekaman.id_peminjamanstudiorekaman=transaksistudiorekaman.id_peminjamanstudiorekaman');
      $this->db->join('studiorekaman','studiorekaman.id_studiorekaman=peminjamanstudiorekaman.id_studiorekaman');
        $this->db->join('sesistudiorekaman','sesistudiorekaman.id_sesistudiorekaman=peminjamanstudiorekaman.id_sesistudiorekaman');
        $this->db->join('user','user.id_user=peminjamanstudiorekaman.id_user');
        $this->db->order_by('peminjamanstudiorekaman.tanggal','DESC');
        return $this->db->get('transaksistudiorekaman')->result();  
    }
    function cetak($id){
        $this->db->join('peminjamanstudiorekaman','peminjamanstudiorekaman.id_peminjamanstudiorekaman=transaksistudiorekaman.id_peminjamanstudiorekaman');
      $this->db->join('studiorekaman','studiorekaman.id_studiorekaman=peminjamanstudiorekaman.id_studiorekaman');
        $this->db->join('sesistudiorekaman','sesistudiorekaman.id_sesistudiorekaman=peminjamanstudiorekaman.id_sesistudiorekaman');
        $this->db->join('user','user.id_user=peminjamanstudiorekaman.id_user');
        $this->db->where('transaksistudiorekaman.id_transaksistudiorekaman', $id);
        return $this->db->get('transaksistudiorekaman')->result();    
    }
    function laporan($bulan){
       $this->db->join('peminjamanstudiorekaman','peminjamanstudiorekaman.id_peminjamanstudiorekaman=transaksistudiorekaman.id_peminjamanstudiorekaman');
      $this->db->join('studiorekaman','studiorekaman.id_studiorekaman=peminjamanstudiorekaman.id_studiorekaman');
        $this->db->join('sesistudiorekaman','sesistudiorekaman.id_sesistudiorekaman=peminjamanstudiorekaman.id_sesistudiorekaman');
        $this->db->join('user','user.id_user=peminjamanstudiorekaman.id_user');
        $this->db->like('peminjamanstudiorekaman.tanggal', $bulan);
        return $this->db->get('transaksistudiorekaman')->result();   
    }
}
?>