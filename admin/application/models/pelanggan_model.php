<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class pelanggan_model extends CI_Model{
    function __construct() {
        parent::__construct();
    }

    function get_all() {
        
        $this->db->like('email','@gmail.com');
        return $this->db->get('user')->result();    
    }

    function get_by($id_user) {
        $this->db->where('id_user', $id_user);
        return $this->db->get('user')->row();    
    }

    // function create($data) {
    //     $this->db->insert('pelanggan', $data);
    //     return $this->db->get('pelanggan')->row();
    // }

    // function update($id_pelanggan, $data) {
    //     $this->db->where('id_pelanggan', $id_pelanggan);
    //     $this->db->update('pelanggan', $data);
    // }

    // function delete($id_pelanggan) {
    //     $this->db->where('id_pelanggan', $id_pelanggan);
    //     $this->db->delete('pelanggan');
    // }
    
}
?>