<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class dashboard_model extends CI_Model{
    function __construct() {
        parent::__construct();
    }
    function user() {
        return $this->db->count_all('user');
    }
    function studiomusik() {
        return $this->db->count_all('studiomusik');
    }
    function studiorekaman() {
        return $this->db->count_all('studiorekaman');
    }
    function sewastudiomusik() {
        $this->db->where('status','Disewa');
        return $this->db->count_all_results('peminjamanstudiomusik');
    }
    function batalstudiomusik() {
        $this->db->where('status','Menunggu Konfirmasi Pembatalan');
        return $this->db->count_all_results('peminjamanstudiomusik');
    }
    function gagalstudiomusik() {
        $this->db->where('status','Sewa Dibatalkan');
        return $this->db->count_all_results('peminjamanstudiomusik');
    }
    function berhasilsewastudiomusik() {
        $this->db->where('status','Berhasil');
        return $this->db->count_all_results('peminjamanstudiomusik');
    }
     function sewastudiorekaman() {
        $this->db->where('status','Disewa');
        return $this->db->count_all_results('peminjamanstudiorekaman');
    }
    function batalstudiorekaman() {
        $this->db->where('status','Menunggu Konfirmasi Pembatalan');
        return $this->db->count_all_results('peminjamanstudiorekaman');
    }
    function gagalstudiorekaman() {
        $this->db->where('status','Sewa Dibatalkan');
        return $this->db->count_all_results('peminjamanstudiorekaman');
    }
    function berhasilsewastudiorekaman() {
        $this->db->where('status','Berhasil');
        return $this->db->count_all_results('peminjamanstudiorekaman');
    }
}
?>