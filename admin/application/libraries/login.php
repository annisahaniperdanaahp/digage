<?php 
if(! defined('BASEPATH')) exit('Akses langsung tidak diperbolehkan'); 
// session_start(); 
class login{
	// SET SUPER GLOBAL
	protected $CI = NULL;

	public function __construct() {
		$this->CI =& get_instance();
		
		$this->CI->load->model('login_model');
	}


	public function cek_login() {
		if($this->CI->session->userdata('email') == NULL) {
			// $this->CI->session->set_flashdata('sukses','Anda berhasil login');
			redirect(site_url('login_admin'));
		}
	}
	// Fungsi login
	public function login($email, $password) {
		$cek_login = $this->CI->login_model->cek_login($email, $password);

		

	if($cek_login->num_rows() == 1) {
		$data = $this->CI->login_model->cek_login($email, $password);
		// echo "<pre>";
  //       print_r($data);
  //       // print_r($email);
  //       // print_r($password);
  //       echo "</pre>";
  //       exit();
		$nama = $data->row()->nama;
		$email = $data->row()->email;
			$this->CI->session->set_userdata('nama', $nama);
			$this->CI->session->set_userdata('email', $email);

			return TRUE;

			}else{
				$this->CI->session->set_flashdata('alert', 'alert("Email atau password salah!");');

			return FALSE;
			}
		}

	// Fungsi logout
	public function logout() {
		$this->CI->session->unset_userdata('nama');
		$this->CI->session->unset_userdata('email');
		// $this->CI->session->set_flashdata('sukses','Anda berhasil logout');
		redirect(base_url('login_admin'));
	}
}