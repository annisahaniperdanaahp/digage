<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class peminjamanstudiorekaman extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->login->cek_login();
		$this->load->library('dompdf_gen');
		$this->load->model('peminjamanstudiorekaman_model');
	}

	public function index() {
		$data = array(
			'title' 			=> 'Data Studio Musik Digage',
			'sidebar' 			=> 'peminjamanstudiorekaman',

			'action'			=> site_url('peminjamanstudiorekaman/create_action'),
			'action1'			=> site_url('peminjamanstudiorekaman/update_action'),
			'studiom'				=> $this->peminjamanstudiorekaman_model->studiorekaman(),
			'sesistudiom'			=> $this->peminjamanstudiorekaman_model->sesistudiorekaman(),
			'peminjamanstudiorekaman' => $this->peminjamanstudiorekaman_model->get_all_transaksi(),
		);

		// echo "<pre>";
		// print_r($data);
		// echo "<pre>";
		// exit();

		$this->template->display('studiorekaman/peminjamanstudiorekaman_list', $data);
	}

	public function dashboard() {
		$data = array(
			'sidebar' => 'dashboard',
		);

		$this->template->display('dashboard', $data);
	}

	// Manajemen Data
	public function create() {
		$data = array(
				'title' 	    => 'Tambah',
				'sidebar' 	    => 'peminjamanstudiorekaman',

				'action' 	    => site_url('peminjamanstudiorekaman/create_action'),
				'id_peminjamanstudiorekaman'		=> set_value('id_peminjamanstudiorekaman'),
				'id_studiorekaman' 				=> set_value('id_studiorekaman'),
				'id_sesistudiorekaman'			=> set_value('id_sesistudiorekaman'),
				'tanggal'						=> set_value('tanggal'),



			);

		$this->template->display('peminjamanstudiorekaman/peminjamanstudiorekaman_form', $data);
	}
        public function confirm($id) {
		// $id_peminjamanstudiorekaman = $this->input->post('id_peminjamanstudiorekaman');
                $query = $this->peminjamanstudiorekaman_model->cetak($id);
                foreach ($query as $key => $value) {
			$id_peminjamanstudiorekaman = $value->id_peminjamanstudiorekaman;
		}
                  $data = array(
				'keterangan' 	=> 'Lunas',
		);
                $this->peminjamanstudiorekaman_model->confirm_transaksi($id, $data);
                
		$data = array(
				'status' 	=> 'Berhasil',
		);
		$this->peminjamanstudiorekaman_model->confirm($id_peminjamanstudiorekaman, $data); 
		redirect(site_url('peminjamanstudiorekaman'));
	}
        	public function confirm_pembatalan($id_peminjamanstudiorekaman) {
		$data = array(
				'status' 	=> 'Sewa Dibatalkan',
		);
		$this->peminjamanstudiorekaman_model->confirm($id_peminjamanstudiorekaman, $data); 
		redirect(site_url('peminjamanstudiorekaman'));
	}
	public function create_action() {
		$sesi =$this->input->post('id_sesistudiorekaman');
		$sesi2 =implode(",", $sesi);
		$data = array(
				'id_studiorekaman' 				=> $this->input->post('id_studiorekaman'),
				'id_sesistudiorekaman' 			=> $sesi2,
				'id_user'						=> $this->session->userdata('id_user'),
				'tanggal'						=> $this->input->post('tanggal'),

			);
		// 	echo "<pre>";
		// print_r($data);
		// echo "<pre>";
		// exit();
		$this->peminjamanstudiorekaman_model->create($data);
		redirect(site_url('peminjamanstudiorekaman'));
	}

	public function update($id_peminjamanstudiorekaman) {
		$peminjamanstudiorekaman = $this->peminjamanstudiorekaman_model->get_by($id_peminjamanstudiorekaman);

		$data = array(
				'title' 	=> 'Edit',
				'sidebar' 	=> 'peminjamanstudiorekaman',

				'action' 		=> site_url('peminjamanstudiorekaman/update_action'),
				'id_peminjamanstudiorekaman'=> set_value('id_peminjamanstudiorekaman', $peminjamanstudiorekaman->id_peminjamanstudiorekaman),
				'peminjamanstudiorekaman' 	=> set_value('nama_studio', $peminjamanstudiorekaman->nama_studio),

			);

		$this->template->display('peminjamanstudiorekaman/peminjamanstudiorekaman_form', $data);
	}

	public function update_action() {
		$id_peminjamanstudiorekaman = $this->input->post('id_peminjamanstudiorekaman');

		$data = array(
				'nama_studio' 	=> $this->input->post('nama_studio'),
		);

		$this->peminjamanstudiorekaman_model->update($id_peminjamanstudiorekaman, $data);
		redirect(site_url('peminjamanstudiorekaman'));
	}

	public function delete($id_peminjamanstudiorekaman) {
		$this->peminjamanstudiorekaman_model->delete($id_peminjamanstudiorekaman);

		redirect(site_url('peminjamanstudiorekaman'));
	}
	public function pdf($id){


		$query = $this->peminjamanstudiorekaman_model->cetak($id.'transaksistudiorekaman');
		
		foreach ($query as $key => $value) {
			$namapemesan = $value->nama;
			$nama_studio = $value->nama_studio;
			$tanggal = $value->tanggal;
			$namasesi = $value->namasesi;
			$jammulai = $value->jammulai;
			$jamakhir = $value->jamakhir;
			$fasilitas = $value->fasilitas;
			$total_harga = $value->total_harga;
			$keterangan = $value->keterangan;
		}
		$data = array(
			'namapemesan'=>$namapemesan,
			'nama_studio'=>$nama_studio,
			'tanggal'=>$tanggal,
			'namasesi'=>$namasesi,
			'jammulai'=>$jammulai,
			'jamakhir'=>$jamakhir,
			'fasilitas'=>$fasilitas,
			'total_harga'=>$total_harga,
			'keterangan'=>$keterangan,
			'peminjamanstudiorekaman'=>$query
		);
		$this->load->view('studiorekaman/buktisewa', $data);

		$paper_size = 'A5';
		$orientation = 'potrait';
		$html = $this->output->get_output();
		$this->dompdf->set_paper($paper_size, $orientation);

		$this->dompdf->load_html($html);
		$this->dompdf->render();
		$this->dompdf->stream("buktisewa.pdf", array('Attachment' =>0)
		);
	}
	public function cetak(){
		$bulan = $this->input->post('bulan');
		$bro = explode('-', $bulan);
		$bulan2 = array (
		1 =>   'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
		);
		$bulan1 = $bulan2[(int)$bro[1]];
		$data=array(
			'laporan'=>$this->peminjamanstudiorekaman_model->laporan($bulan),
			'bulan'=>$bulan1. ' '.$bro[0],
		);
		$this->load->view('studiorekaman/laporan', $data);

		$paper_size = 'A4';
		$orientation = 'landscape';
		$html = $this->output->get_output();
		$this->dompdf->set_paper($paper_size, $orientation);

		$this->dompdf->load_html($html);
		$this->dompdf->render();
		$this->dompdf->stream("buktisewa.pdf", array('Attachment' =>0)
		);
		
	}
}