<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class sesistudiorekaman extends CI_Controller {

	function __construct(){
		parent::__construct();
	
		$this->load->model('sesistudiorekaman_model');
	}

	public function index() {
		$data = array(
			'title' 			=> 'Data Studio Musik Digage',
			'sidebar' 			=> 'sesistudiorekaman',

			'action'			=> site_url('sesistudiorekaman/create_action'),
			'action1'			=> site_url('sesistudiorekaman/update_action'),
			'sesistudiorekaman' 	=> $this->sesistudiorekaman_model->get_all(),
		);

		$this->template->display('studiorekaman/sesistudiorekaman_list', $data);
	}

	public function dashboard() {
		$data = array(
			'sidebar' => 'dashboard',
		);

		$this->template->display('dashboard', $data);
	}

	// Manajemen Data
	public function create() {
		$data = array(
				'title' 	    => 'Tambah',
				'sidebar' 	    => 'sesistudiorekaman',

				'action' 	   		 	=> site_url('sesistudiorekaman/create_action'),
				'id_sesistudiorekaman'	=> set_value('id_sesistudiorekaman'),
				'namasesi'				=> set_value('namasesi'),
				'jammulai'				=> set_value('jammulai'),
				'jamakhir'				=> set_value('jamakhir'),
				
			);

		$this->template->display('sesistudiorekaman/sesistudiorekaman_form', $data);
	}

	public function create_action() {
		$data = array(
				'namasesi' 		=> $this->input->post('namasesi'),
				'jammulai' 		=> $this->input->post('jammulai'),
				'jamakhir' 		=> $this->input->post('jamakhir'),
			);
		
		$this->sesistudiorekaman_model->create($data);
		redirect(site_url('sesistudiorekaman'));
	}

	public function update($id_sesistudiorekaman) {
		$sesistudiorekaman = $this->sesistudiorekaman_model->get_by($id_sesistudiorekaman);

		$data = array(
				'title' 	=> 'Edit',
				'sidebar' 	=> 'sesistudiorekaman',

				'action' 			=> site_url('sesistudiorekaman/update_action'),
				'action1'			=> site_url('sesistudiorekaman/update_action'),
				'id_sesistudiorekaman'=> set_value('id_sesistudiorekaman', $sesistudiorekaman->id_sesistudiorekaman),
			);

		$this->template->display('sesistudiorekaman/sesistudiorekaman_form', $data);
	}

	public function update_action() {
		

                $id_sesistudiorekaman = $this->input->post('id_sesistudiorekaman');
		$data = array(
				'id_sesistudiorekaman'    => $this->input->post('id_sesistudiorekaman'),
				'namasesi' 		=> $this->input->post('namasesi'),
				'jammulai' 		=> $this->input->post('jammulai'),
				'jamakhir' 		=> $this->input->post('jamakhir'),
			);
		$this->sesistudiorekaman_model->update($id_sesistudiorekaman,$data);
		redirect(site_url('sesistudiorekaman'));
	}

	public function delete($id_sesistudiorekaman) {
		$this->sesistudiorekaman_model->delete($id_sesistudiorekaman);

		redirect(site_url('sesistudiorekaman'));
	}
}