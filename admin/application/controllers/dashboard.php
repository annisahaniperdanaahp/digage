<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class dashboard extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->login->cek_login();
        $this->load->model('dashboard_model'); 



    }

    public function index() {
        $data = array(
            'title'             => 'Data Alat Musik Digage',
            'sidebar'           => 'dashboard',
            'user'           => $this->dashboard_model->user(),
            'studiomusik'       => $this->dashboard_model->studiomusik(),
            'studiorekaman'     => $this->dashboard_model->studiorekaman(),
            'sewastudiomusik'   => $this->dashboard_model->sewastudiomusik(),
            'batalstudiomusik'   => $this->dashboard_model->batalstudiomusik(),
            'gagalstudiomusik'   => $this->dashboard_model->gagalstudiomusik(),
            'berhasilstudiomusik'   => $this->dashboard_model->berhasilsewastudiomusik(),
            'sewastudiorekaman'   => $this->dashboard_model->sewastudiorekaman(),
            'batalstudiorekaman'   => $this->dashboard_model->batalstudiorekaman(),
            'gagalstudiorekaman'   => $this->dashboard_model->gagalstudiorekaman(),
            'berhasilstudiorekaman'   => $this->dashboard_model->berhasilsewastudiorekaman(),
        );
        $this->template->display('dashboard', $data);
    }
}