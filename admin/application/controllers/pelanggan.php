<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class pelanggan extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->login->cek_login();

		$this->load->model('pelanggan_model');
	}

	public function index() {
		$data = array(
			'title' 			=> 'Data Studio Musik Digage',
			'sidebar' 			=> 'pelanggan',

			'action'			=> site_url('pelanggan/create_action'),
			'action1'			=> site_url('pelanggan/update_action'),
			'pelanggan'                     => $this->pelanggan_model->get_all(),
		);

		$this->template->display('pelanggan_list', $data);
	}

	public function dashboard() {
		$data = array(
			'sidebar' => 'dashboard',
		);

		$this->template->display('dashboard', $data);
	}

	
}