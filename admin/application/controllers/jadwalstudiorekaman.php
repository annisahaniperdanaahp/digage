<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class jadwalstudiorekaman extends CI_Controller {

	function __construct(){
		parent::__construct();

		$this->load->model('jadwalstudiorekaman_model');
		$this->load->model('studiorekaman_model'); 
	}

	public function index() {
		$data = array(
			'title' 			=> 'Data Studio Musik Digage',
			'sidebar' 			=> 'jadwalstudiorekaman',

			'jadwalstudiorekaman' => $this->jadwalstudiorekaman_model->get_all(),
			'kalender'			=> $this->jadwalstudiorekaman_model->eventlist(),
		);

		$this->template->display('studiorekaman/jadwalstudiorekaman_list', $data);
	}
	public function cek(){
		$tanggal= $this->input->post('tanggal');
		$id_sesistudiomusik= $this->input->post('id_sesistudiomusik');
		$id_studiomusik= $this->input->post('id_studiomusik');
		$data=$this->jadwalstudiomusik_model->cek($tanggal,$id_studiomusik,$id_sesistudiomusik);
		echo json_encode($data);
	}
	public function dashboard() {
		$data = array(
			'sidebar' => 'dashboard',
		);

		$this->template->display('dashboard', $data);
	}

}