<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class peminjamanstudiomusik extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->login->cek_login();
		$this->load->library('dompdf_gen');
		$this->load->model('peminjamanstudiomusik_model');
	}

	public function index() {
		$data = array(
			'title' 			=> 'Data Studio Musik Digage',
			'sidebar' 			=> 'peminjamanstudiomusik',

			'action'			=> site_url('peminjamanstudiomusik/create_action'),
			'action1'			=> site_url('peminjamanstudiomusik/update_action'),
			'studiom'				=> $this->peminjamanstudiomusik_model->studiomusik(),
			'sesistudiom'			=> $this->peminjamanstudiomusik_model->sesistudiomusik(),
			'peminjamanstudiomusik' => $this->peminjamanstudiomusik_model->get_peminjaman(),
		);


		$this->template->display('studiomusik/peminjamanstudiomusik_list', $data);
	}

	public function dashboard() {
		$data = array(
			'sidebar' => 'dashboard',
		);

		$this->template->display('dashboard', $data);
	}

	// Manajemen Data
	public function create() {
		$data = array(
				'title' 	    => 'Tambah',
				'sidebar' 	    => 'peminjamanstudiomusik',

				'action' 	    => site_url('peminjamanstudiomusik/create_action'),
				'id_peminjamanstudiomusik'		=> set_value('id_peminjamanstudiomusik'),
				'id_studiomusik' 				=> set_value('id_studiomusik'),
				'id_sesistudiomusik'			=> set_value('id_sesistudiomusik'),
				'tanggal'						=> set_value('tanggal'),



			);

		$this->template->display('peminjamanstudiomusik/peminjamanstudiomusik_form', $data);
	}

	public function create_action() {
		$sesi =$this->input->post('id_sesistudiomusik');
		$sesi2 =implode(",", $sesi);
		$data = array(
				'id_studiomusik' 				=> $this->input->post('id_studiomusik'),
				'id_sesistudiomusik' 			=> $sesi2,
				'id_user'						=> $this->session->userdata('id_user'),
				'tanggal'						=> $this->input->post('tanggal'),

			);

		$this->peminjamanstudiomusik_model->create($data);
		redirect(site_url('peminjamanstudiomusik'));
	}
        public function update_sesi() {
		$data = array(
				'id_studiomusik' 				=> $this->input->post('id_studiomusik'),
				'namasesi' 			=> $this->input->post('namasesi'),
				'jammulai'						=> $this->session->userdata('jammulai'),
				'jamakhir'						=> $this->input->post('jamakhir'),

			);

                print_r($data);exit();
		$this->peminjamanstudiomusik_model->create($data);
		redirect(site_url('peminjamanstudiomusik'));
	}
	public function update($id_peminjamanstudiomusik) {
		$peminjamanstudiomusik = $this->peminjamanstudiomusik_model->get_by($id_peminjamanstudiomusik);

		$data = array(
				'title' 	=> 'Edit',
				'sidebar' 	=> 'peminjamanstudiomusik',

				'action' 		=> site_url('peminjamanstudiomusik/update_action'),
				'id_peminjamanstudiomusik'=> set_value('id_peminjamanstudiomusik', $peminjamanstudiomusik->id_peminjamanstudiomusik),
				'peminjamanstudiomusik' 	=> set_value('nama_studio', $peminjamanstudiomusik->nama_studio),

			);

		$this->template->display('peminjamanstudiomusik/peminjamanstudiomusik_form', $data);
	}

	public function confirm($id) {
                $query = $this->peminjamanstudiomusik_model->cetak($id.'transaksistudiomusik');
                foreach ($query as $key => $value) {
			$id_peminjamanstudiomusik = $value->id_peminjamanstudiomusik;
		}
                  $data = array(
				'keterangan' 	=> 'Lunas',
		);
                $this->peminjamanstudiomusik_model->confirm_transaksi($id, $data);
		$data = array(
				'status' 	=> 'Berhasil',
		);
		$this->peminjamanstudiomusik_model->confirm($id_peminjamanstudiomusik, $data); 
		redirect(site_url('peminjamanstudiomusik'));
	}
        	public function confirm_pembatalan($id_peminjamanstudiomusik) {

		$data = array(
				'status' 	=> 'Sewa Dibatalkan',
		);

		$this->peminjamanstudiomusik_model->confirm($id_peminjamanstudiomusik, $data); 
		redirect(site_url('peminjamanstudiomusik'));
	}

	public function update_action() {
		$id_peminjamanstudiomusik = $this->input->post('id_peminjamanstudiomusik');

		$data = array(
				'nama_studio' 	=> $this->input->post('nama_studio'),
		);

		$this->peminjamanstudiomusik_model->update($id_peminjamanstudiomusik, $data);
		redirect(site_url('peminjamanstudiomusik'));
	}

	public function delete($id_peminjamanstudiomusik) {
		$this->peminjamanstudiomusik_model->delete($id_peminjamanstudiomusik);

		redirect(site_url('peminjamanstudiomusik'));
	}
	public function pdf($id){
		
		$query = $this->peminjamanstudiomusik_model->cetak($id.'transaksistudiomusik');
		foreach ($query as $key => $value) {
			$namapemesan = $value->nama;
			$nama_studio = $value->nama_studio;
			$tanggal = $value->tanggal;
			$namasesi = $value->namasesi;
			$jammulai = $value->jammulai;
			$jamakhir = $value->jamakhir;
			$fasilitas = $value->fasilitas;
			$total_harga = $value->total_harga;
			$keterangan = $value->keterangan;
		}
		$data = array(
			'namapemesan'=>$namapemesan,
			'nama_studio'=>$nama_studio,
			'tanggal'=>$tanggal,
			'namasesi'=>$namasesi,
			'jammulai'=>$jammulai,
			'jamakhir'=>$jamakhir,
			'fasilitas'=>$fasilitas,
			'total_harga'=>$total_harga,
			'keterangan'=>$keterangan,
			'peminjamanstudiomusik'=>$query
		);
		$this->load->view('studiomusik/buktisewa', $data);

		$paper_size = 'A5';
		$orientation = 'potrait';
		$html = $this->output->get_output();
		$this->dompdf->set_paper($paper_size, $orientation);

		$this->dompdf->load_html($html);
		$this->dompdf->render();
		$this->dompdf->stream("buktisewa.pdf", array('Attachment' =>0)
		);
	}
	public function cetak(){
		$bulan = $this->input->post('bulan');
		$bro = explode('-', $bulan);
		$bulan2 = array (
		1 =>   'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
		);
		$bulan1 = $bulan2[(int)$bro[1]];
		$data=array(
			'laporan'=>$this->peminjamanstudiomusik_model->laporan($bulan),
			'bulan'=>$bulan1. ' '.$bro[0],
		);
		$this->load->view('studiomusik/laporan', $data);

		$paper_size = 'A4';
		$orientation = 'landscape';
		$html = $this->output->get_output();
		$this->dompdf->set_paper($paper_size, $orientation);

		$this->dompdf->load_html($html);
		$this->dompdf->render();
		$this->dompdf->stream("laporan_".$bulan.".pdf", array('Attachment' =>0)
		);
		
	}
}