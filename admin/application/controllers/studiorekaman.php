<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class studiorekaman extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->login->cek_login();

		$this->load->model('studiorekaman_model');
	}

	public function index() {
		$data = array(
			'title' 			=> 'Data Studio Musik Digage',
			'sidebar' 			=> 'studiorekaman',

			'action'			=> site_url('studiorekaman/create_action'),
			'action1'			=> site_url('studiorekaman/update_action'),
			'studiorekaman' 		=> $this->studiorekaman_model->get_all(),
		);

		$this->template->display('studiorekaman/studiorekaman_list', $data);
	}

	public function dashboard() {
		$data = array(
			'sidebar' => 'dashboard',
		);

		$this->template->display('dashboard', $data);
	}

	// Manajemen Data
	public function create() {
		$data = array(
				'title' 	    => 'Tambah',
				'sidebar' 	    => 'studiorekaman',

				'action' 	    => site_url('studiorekaman/create_action'),
				'id_studiorekaman'	=> set_value('id_studiorekaman'),
				'studiorekaman' 		=> set_value('nama_studio'),
				'harga'				=> set_value('harga'),
				'fasilitas'			=> set_value('fasilitas'),

			);

		$this->template->display('studiorekaman/studiorekaman_form', $data);
	}

	public function create_action() {
		$data = array(
				'nama_studio' 		=> $this->input->post('nama_studio'),
				'harga' 			=> $this->input->post('harga'),
				'fasilitas'			=> $this->input->post('fasilitas'),
			);
		
		$this->studiorekaman_model->create($data);
		redirect(site_url('studiorekaman'));
	}

	public function update($id_studiorekaman) {
		$studiorekaman = $this->studiorekaman_model->get_by($id_studiorekaman);

		$data = array(
				'title' 	=> 'Edit',
				'sidebar' 	=> 'studiorekaman',

				'action' 		=> site_url('studiorekaman/update_action'),
				'id_studiorekaman'=> set_value('id_studiorekaman', $studiorekaman->id_studiorekaman),
				'nama_studio' 	=> set_value('nama_studio', $studiorekaman->nama_studio),
				'harga' 		=> set_value('harga', $studiorekaman->harga),
				'fasilitas' 	=> set_value('fasilitas', $studiorekaman->fasilitas),
			);

		$this->template->display('studiorekaman/studiorekaman_form', $data);
	}

	public function update_action() {
		$id_studiorekaman = $this->input->post('id_studiorekaman');

		$data = array(
				'nama_studio' 	=> $this->input->post('nama_studio'),
				'harga' 		=> $this->input->post('harga'),
				'fasilitas' 	=> $this->input->post('fasilitas'),
		);


		$this->studiorekaman_model->update($id_studiorekaman, $data);
		redirect(site_url('studiorekaman'));
	}

	public function delete($id_studiorekaman) {
		$this->studiorekaman_model->delete($id_studiorekaman);

		redirect(site_url('studiorekaman'));
	}
}