<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class studiomusik extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->login->cek_login();

		$this->load->model('studiomusik_model');
	}

	public function index() {
		$data = array(
			'title' 			=> 'Data Studio Musik Digage',
			'sidebar' 			=> 'studiomusik',

			'action'			=> site_url('studiomusik/create_action'),
			'action1'			=> site_url('studiomusik/update_action'),
			'studiomusik' 		=> $this->studiomusik_model->get_all(),
		);

		$this->template->display('studiomusik/studiomusik_list', $data);
	}

	public function dashboard() {
		$data = array(
			'sidebar' => 'dashboard',
		);

		$this->template->display('dashboard', $data);
	}

	// Manajemen Data
	public function create() {
		$data = array(
				'title' 	    => 'Tambah',
				'sidebar' 	    => 'studiomusik',

				'action' 	    => site_url('studiomusik/create_action'),
				'id_studiomusik'	=> set_value('id_studiomusik'),
				'studiomusik' 		=> set_value('nama_studio'),
				'harga'				=> set_value('harga'),
				'fasilitas'			=> set_value('fasilitas'),

			);

		$this->template->display('studiomusik/studiomusik_form', $data);
	}

	public function create_action() {
		$data = array(
				'nama_studio' 		=> $this->input->post('nama_studio'),
				'harga' 			=> $this->input->post('harga'),
				'fasilitas'			=> $this->input->post('fasilitas'),
			);
		
		$this->studiomusik_model->create($data);
		redirect(site_url('studiomusik'));
	}

	public function update($id_studiomusik) {
		$studiomusik = $this->studiomusik_model->get_by($id_studiomusik);

		$data = array(
				'title' 	=> 'Edit',
				'sidebar' 	=> 'studiomusik',

				'action' 		=> site_url('studiomusik/update_action'),
				'id_studiomusik'=> set_value('id_studiomusik', $studiomusik->id_studiomusik),
				'nama_studio' 	=> set_value('nama_studio', $studiomusik->nama_studio),
				'harga' 		=> set_value('harga', $studiomusik->harga),
				'fasilitas' 	=> set_value('fasilitas', $studiomusik->fasilitas),
			);

		$this->template->display('studiomusik/studiomusik_form', $data);
	}

	public function update_action() {
		$id_studiomusik = $this->input->post('id_studiomusik');

		$data = array(
				'nama_studio' 	=> $this->input->post('nama_studio'),
				'harga' 	=> $this->input->post('harga'),
				'fasilitas' 	=> $this->input->post('fasilitas'),
		);


		$this->studiomusik_model->update($id_studiomusik, $data);
		redirect(site_url('studiomusik'));
	}

	public function delete($id_studiomusik) {
		$this->studiomusik_model->delete($id_studiomusik);

		redirect(site_url('studiomusik'));
	}
}