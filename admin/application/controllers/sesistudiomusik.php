<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class sesistudiomusik extends CI_Controller {

	function __construct(){
		parent::__construct();

		$this->load->model('sesistudiomusik_model');
	}

	public function index() {
		$data = array(
			'title' 			=> 'Data Studio Musik Digage',
			'sidebar' 			=> 'sesistudiomusik',

			'action'			=> site_url('sesistudiomusik/create_action'),
			'action1'			=> site_url('sesistudiomusik/update_action'),
			'sesistudiomusik' 	=> $this->sesistudiomusik_model->get_all(),
		);

		$this->template->display('studiomusik/sesistudiomusik_list', $data);
	}

	public function dashboard() {
		$data = array(
			'sidebar' => 'dashboard',
		);

		$this->template->display('dashboard', $data);
	}

	// Manajemen Data
	public function create() {
		$data = array(
				'title' 	    => 'Tambah',
				'sidebar' 	    => 'sesistudiomusik',

				'action' 	   		 	=> site_url('sesistudiomusik/create_action'),
				'id_sesistudiomusik'	=> set_value('id_sesistudiomusik'),
				'namasesi'				=> set_value('namasesi'),
				'jammulai'				=> set_value('jammulai'),
				'jamakhir'				=> set_value('jamakhir'),
				
			);

		$this->template->display('sesistudiomusik/sesistudiomusik_form', $data);
	}

	public function create_action() {
           	$data = array(
				'namasesi' 		=> $this->input->post('namasesi'),
				'jammulai' 		=> $this->input->post('jammulai'),
				'jamakhir' 		=> $this->input->post('jamakhir'),
			);
		
		$this->sesistudiomusik_model->create($data);
		redirect(site_url('sesistudiomusik'));
	}
	public function update_action() {
            $id_sesistudiomusik = $this->input->post('id_sesistudiomusik');
		$data = array(
				'id_sesistudiomusik'    => $this->input->post('id_sesistudiomusik'),
				'namasesi' 		=> $this->input->post('namasesi'),
				'jammulai' 		=> $this->input->post('jammulai'),
				'jamakhir' 		=> $this->input->post('jamakhir'),
			);
		$this->sesistudiomusik_model->update($id_sesistudiomusik,$data);
		redirect(site_url('sesistudiomusik'));
	}

	public function update($id_sesistudiomusik) {
		$sesistudiomusik = $this->sesistudiomusik_model->get_by($id_sesistudiomusik);

		$data = array(
				'title' 	=> 'Edit',
				'sidebar' 	=> 'sesistudiomusik',

				'action' 			=> site_url('sesistudiomusik/update_action'),
				'action1'			=> site_url('sesistudiomusik/update_action'),
				'id_sesistudiomusik'=> set_value('id_sesistudiomusik', $sesistudiomusik->id_sesistudiomusik),
			);

		$this->template->display('sesistudiomusik/sesistudiomusik_form', $data);
	}


	public function delete($id_sesistudiomusik) {
		$this->sesistudiomusik_model->delete($id_sesistudiomusik);

		redirect(site_url('sesistudiomusik'));
	}
}