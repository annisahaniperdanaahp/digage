<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class jadwalstudiomusik extends CI_Controller {

	function __construct(){
		parent::__construct();

		$this->load->model('jadwalstudiomusik_model');
		$this->load->model('studiomusik_model'); 

	}

	public function index() {
		$data = array(
			'title' 			=> 'Data Studio Musik Digage',
			'sidebar' 			=> 'jadwalstudiomusik',

			'jadwalstudiomusik' => $this->jadwalstudiomusik_model->get_all(),
			'kalender'			=> $this->jadwalstudiomusik_model->eventlist(),
		);

		$this->template->display('studiomusik/jadwalstudiomusik_list', $data);
	}

	public function dashboard() {
		$data = array(
			'sidebar' => 'dashboard',
		);

		$this->template->display('dashboard', $data);
	}

	
}