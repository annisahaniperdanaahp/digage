    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url('dashboard') ?>">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-4 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h5>Jumlah user</h5>

                <p><h3><?= $user ?></h3></p>
              </div>
                <div class="icon">
                <i class="fas fa-user"></i>
              </div>
              <a href="<?php echo base_url('pelanggan') ?>" class="small-box-footer">User <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
            <div class="col-lg-4 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h5>Jumlah Studio Musik</h5>

                <p><h3><?= $studiomusik ?></h3></p>
              </div>
                <div class="icon">
                <i class="fas fa-music"></i>
              </div>
              <a href="<?php echo base_url('studiomusik') ?>" class="small-box-footer">Studio Musik <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
            <div class="col-lg-4 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h5>Jumlah Studio Rekaman</h5>

                <p><h3><?= $studiorekaman ?></h3></p>
              </div>
                 <div class="icon">
                <i class="fas fa-home"></i>
              </div>
              <a href="<?php echo base_url('studiorekaman') ?>" class="small-box-footer">Studio Rekaman <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h5>Penyewaan</h5>

                <p><h3><?= $sewastudiomusik ?></h3></p>
              </div>
                 <div class="icon">
                <i class="fas fa-music"></i>
              </div>
              <a href="<?php echo base_url('studiomusik') ?>" class="small-box-footer">Studio Musik <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
                <h5>Pembatalan</h5>

                <p><h3><?= $batalstudiomusik ?></h3></p>
              </div>
                 <div class="icon">
                <i class="fas fa-music"></i>
              </div>
              <a href="<?php echo base_url('studiomusik') ?>" class="small-box-footer">Studio Musik <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-danger">
              <div class="inner">
                <h5>Gagal</h5>

                <p><h3><?= $gagalstudiomusik ?></h3></p>
              </div>
                 <div class="icon">
                <i class="fas fa-music"></i>
              </div>
              <a href="<?php echo base_url('studiomusik') ?>" class="small-box-footer">Studio Musik <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h5>Berhasil</h5>

                <p><h3><?= $berhasilstudiomusik ?></h3></p>
              </div>
                 <div class="icon">
                <i class="fas fa-music"></i>
              </div>
              <a href="<?php echo base_url('studiomusik') ?>" class="small-box-footer">Studio Musik <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
              <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h5>Penyewaan</h5>

                <p><h3><?= $sewastudiorekaman ?></h3></p>
              </div>
                 <div class="icon">
                <i class="fas fa-home"></i>
              </div>
              <a href="<?php echo base_url('peminjamanstudiorekaman') ?>" class="small-box-footer">Studio Rekaman <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
              <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
                <h5>Pembatalan</h5>

                <p><h3><?= $batalstudiorekaman ?></h3></p>
              </div>
                 <div class="icon">
                <i class="fas fa-home"></i>
              </div>
              <a href="<?php echo base_url('peminjamanstudiorekaman') ?>" class="small-box-footer">Studio Rekaman <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
              <div class="col-lg-3 col-6">
                  <!-- small box -->
                  <div class="small-box bg-danger">
                    <div class="inner">
                      <h5>Gagal</h5>

                      <p><h3><?= $gagalstudiorekaman?></h3></p>
                    </div>
                       <div class="icon">
                      <i class="fas fa-home"></i>
                    </div>
                    <a href="<?php echo base_url('peminjamanstudiorekaman') ?>" class="small-box-footer">Studio Rekaman <i class="fas fa-arrow-circle-right"></i></a>
                  </div>
                </div>
              <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h5>Berhasil</h5>

                <p><h3><?= $berhasilstudiorekaman ?></h3></p>
              </div>
                 <div class="icon">
                <i class="fas fa-home"></i>
              </div>
              <a href="<?php echo base_url('peminjamanstudiorekaman') ?>" class="small-box-footer">Studio Rekaman <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->