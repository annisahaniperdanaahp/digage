<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Digage Studio Musik</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <?php echo $_header ?>
  
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
  <?php echo $_navbar ?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <?php echo $_sidebar ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <?php echo $_content ?>
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2020 Annisa Hani Perdana</a></strong>
    
    <div class="float-right d-none d-sm-inline-block">
      <b>D3 TEKNIK INFORMATIKA UNS</b>
    </div>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery --><!-- 
<script src=""></script>
jQuery UI 1.11.4
<script src=""></script> -->

<script src="<?php echo base_url('assets/') ?>plugins/jquery/jquery.min.js"></script>
<script src="<?php echo base_url('assets/') ?>plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)/
</script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url('assets/') ?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="<?php echo base_url('assets/') ?>plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo base_url('assets/') ?>plugins/sparklines/sparkline.js"></script>
<!-- JQVMap -->
<script src="<?php echo base_url('assets/') ?>plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="<?php echo base_url('assets/') ?>plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url('assets/') ?>plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?php echo base_url('assets/') ?>plugins/moment/moment.min.js"></script>
<script src="<?php echo base_url('assets/') ?>plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<?php echo base_url('assets/') ?>plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url('assets/') ?>plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url('assets/') ?>plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<!-- Select2 -->
<script src="<?php echo base_url('assets/') ?>plugins/select2/js/select2.full.min.js"></script>
<!-- Summernote -->
<script src="<?php echo base_url('assets/') ?>plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="<?php echo base_url('assets/') ?>plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- SweetAlert2 -->
<script src="<?php echo base_url('assets/') ?>plugins/sweetalert2/sweetalert2.min.js"></script>
<script src="<?php echo base_url('assets/') ?>plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/') ?>dist/js/adminlte.js"></script>

<script src="<?php echo base_url('assets/fullcalendar/packages/core/main.js') ?>"></script>
<script src="<?php echo base_url('assets/fullcalendar/packages/interaction/main.js') ?>"></script>
<script src="<?php echo base_url('assets/fullcalendar/packages/daygrid/main.js') ?>"></script>
<script src="<?php echo base_url('assets/fullcalendar/packages/timegrid/main.js') ?>"></script>
<script src="<?php echo base_url('assets/fullcalendar/packages/list/main.js') ?>"></script>



<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>
</script>

<script type="text/javascript">
$(document).ready(function () {
  bsCustomFileInput.init();
});
</script>

<script>

document.addEventListener('DOMContentLoaded', function() {
var initialLocaleCode = 'id';
var localeSelectorEl = document.getElementById('locale-selector');
var calendarEl = document.getElementById('calendar');
var event = 

[
    <?php foreach ($kalender->result_array() as $row1) { 

        ?>
    {
      title: '<?php echo $row1['nama']." / ".$row1['nama_studio']." / ".$row1['namasesi'] ?>',
      start: '<?php echo $row1['tanggal']?>'
    },
    <?php } ?>
    
]

var calendar = new FullCalendar.Calendar(calendarEl, {
  plugins: [ 'interaction', 'dayGrid', 'timeGrid', 'list' ],
  header: {
    left: 'prev,next today',
    center: 'title',
    right: 'dayGridMonth,timeGridWeek,timeGridDay,listMonth'
  },
  defaultDate: new Date(),
  locale: initialLocaleCode,
  buttonIcons: false, // show the prev/next text
  weekNumbers: true,
  navLinks: true, // can click day/week names to navigate views
  editable: true,
  eventLimit: true, // allow "more" link when too many events
  events: event,
  eventClick: function(event, element) {
            element.bind('dblclick', function() {
                // $('#ModalEdit #id').val(event.id);
                // $('#ModalEdit #title').val(event.title);
                // $('#ModalEdit #color').val(event.color);
                $('#calendarModal').modal('show');
            });
        },

});

calendar.render();

// build the locale selector's options
calendar.getAvailableLocaleCodes().forEach(function(localeCode) {
  var optionEl = document.createElement('option');
  optionEl.value = localeCode;
  optionEl.selected = localeCode == initialLocaleCode;
  optionEl.innerText = localeCode;
  localeSelectorEl.appendChild(optionEl);
});

// when the selected option changes, dynamically change the calendar option
localeSelectorEl.addEventListener('change', function() {
  if (this.value) {
    calendar.setOption('locale', this.value);
  }
});

});

</script>
</body>
</html>
