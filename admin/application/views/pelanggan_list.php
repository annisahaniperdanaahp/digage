<section class="content-header">
<!--   <h1>
    <?php echo $title ?>
  </h1> -->
</section>

<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
            <div class="card card-primary">
              <div class="card-header">
                <div class="card-title">
                 List Studio Musik
                </div>
                <div class="btn-group float-sm-right">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-musik">
                  Tambah
                </button>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
              <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Nama</th>
                      <th>Email</th>
                      <th>No Telp</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                    $no=0;
                      foreach ($pelanggan as $key => $value) { ?>
                        <tr>
                          <td><?php echo ++$no ?></td>
                          <td><?php echo $value->nama ?></td>
                          <td><?php echo $value->email ?></td>
                          <td><?php echo $value->no_telp ?></td>
                        </tr>

                    <?php
                      }
                    ?>
                  </tbody>
                </table>
              </div>
              <!-- /.box-body -->


<!--  -->
            </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>




