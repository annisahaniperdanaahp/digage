<DOCTYPE html>
	<html>
  <head>
	  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>BUKTI SEWA STUDIO MUSIK</title>
  <link rel="stylesheet" href="">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <style>

    table {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  margin-left: 20px;
    margin-right: 20px;
}

#ini {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#ini td, #ini th {
  border: 3px solid #ddd;
  padding: 8px;
}

#ini tr:nth-child(even){background-color: #f2f2f2;}

#ini tr:hover {background-color: #ddd;}

#ini th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #4CAF50;
  color: white;
}
  </style>
</head>
<body>
 <img src="./img/digage.png" style="position: absolute; width: 60px; height: auto;">
  <table style="width: 100%;">
    <tr>
      <td align="center">
        <span style="line-height: 1.6; font-weight: bold;">
          STUDIO MUSIK DIGAGE
          <br>Jl. Solo-Sukoharjo Jl. Dompilan I, Gabusan, Jombor, Kec. Bendosari, Kabupaten Sukoharjo, Jawa Tengah 57521
        </span>
      </td>
    </tr>
  </table>
   <hr class="line-title"> 
  <p align="center">
    LAPORAN SEWA STUDIO REKAMAN <br>
    <b><?= $bulan ?></b>
  </p>
      <div style="overflow-x:auto;">
    	<table id="ini" class="table table-bordered table-striped">
           
                  <tr>
                    <th><center>No</center></th>
                    <th><center>Nama</center></th>
                    <th><center>Tanggal</center></th>
                    <th><center>Nama Studio</center></th>
                    <th><center>Sesi</center></th>
                    <th><center>Harga Sewa</center></th>
                    <th><center>Status</center></th>
                  </tr>
             
                  <?php 
                  $no=0;
                    foreach ($laporan as $key => $value) { 
                      ?>
                      <tr>
                        <td><?php echo ++$no ?></td>
                       <td><?php echo $value->nama?></td>
                       <td><?php echo $value->tanggal?></td>
                        <td><?php echo $value->nama_studio ?></td>
                        <td><?php echo $value->namasesi ?></td>
                        <td><?php echo $value->harga ?></td>
                       <td>
                         <?php echo $value->status ?>
                        </td>
                      </tr>

                  <?php
                    }
                  ?>
                  
              
              </table>
    </div>
    </body>
    </html>
    