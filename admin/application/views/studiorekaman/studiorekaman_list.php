<section class="content-header">
<!--   <h1>
    <?php echo $title ?>
  </h1> -->
</section>

<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
            <div class="card card-primary">
              <div class="card-header">
                <div class="card-title">
                 List Studio Musik
                </div>
                <div class="btn-group float-sm-right">
              <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-musik">
                  Tambah
                </button>
             </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama Studio</th>
                    <th>Harga Per Sesi</th>
                    <th>Fasilitas</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                  $no=0;
                    foreach ($studiorekaman as $key => $value) { ?>
                      <tr>
                        <td><?php echo ++$no ?></td>
                        <td><?php echo $value->nama_studio ?></td>
                        <td><?php echo $value->harga ?></td>
                        <td><?php echo $value->fasilitas ?></td>
                        <td>
                          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#edit_<?php echo $value->id_studiorekaman ?>">
                            <i class="fas fa-pencil-alt">
                              </i>
                              Edit
                          </button>
                          <!-- <a id="<?php echo $value->id_studiorekaman ?>" class="btn btn-info btn-sm"  data-toogle="modal" data-target="#edit_<?php echo $value->id_studiorekaman ?>">
                              <i class="fas fa-pencil-alt">
                              </i>
                              Edit
                          </a> -->
                         <a type="button" class="btn btn-danger" href="<?php echo site_url('studiorekaman/delete/'.$value->id_studiorekaman) ?>">
                              <i class="fas fa-trash">
                              </i>
                              Hapus
                          </a>
                        </td>
                      </tr>

                  <?php
                    }
                  ?>
                  
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->



      <?php foreach ($studiorekaman as $key1 => $value1) { ?>
      <div class="modal fade" id="edit_<?php echo $value1->id_studiorekaman ?>">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Tambahkan Ruangan Studio Musik</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form action="<?php echo $action1 ?>" method="post">
            <div class="modal-body">
              <div class="form-group">
                <label for="exampleInputEmail1">Nama Ruangan Studio Musik</label>
                <input type="text" name="nama_studio" id="nama_studio" class="form-control" placeholder="Masukkan Ruangan Studio Musik" required autocomplete="off" value="<?php echo $value1->nama_studio ?>" />
                <input type="hidden" name="id_studiorekaman" id="nama_studio" class="form-control" value="<?php echo $value1->id_studiorekaman; ?>" /> 
                <label for="exampleInputEmail1">Harga Per Sesi</label>
                <input type="text" name="harga" id="harga" class="form-control" placeholder="Masukkan Harga Per Sesi Studio Musik" required autocomplete="off" value="<?php echo $value1->harga ?>" />
                <input type="hidden" name="id_studiorekaman" id="harga" class="form-control" value="<?php echo $value1->id_studiorekaman; ?>" /> 
                <label for="exampleInputEmail1">Fasilitas</label>
                <input type="text" name="fasilitas" id="fasilitas" class="form-control" placeholder="Masukkan Fasilitas Studio Musik" required autocomplete="off" value="<?php echo $value1->fasilitas ?>" />
                <input type="hidden" name="id_studiorekaman" id="nama_studio" class="form-control" value="<?php echo $value1->id_studiorekaman; ?>" /> 
              </div>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
              <button type="submit" class="btn btn-primary">Edit</button>
            </div>
            </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
        <?php } ?>
      <div class="modal fade" id="modal-musik">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Tambahkan Ruangan Studio Musik</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form action="<?php echo base_url('studiorekaman/create_action') ?>" method="post">
            <div class="modal-body">
              <label for="exampleInputEmail1">Nama Ruangan Studio Musik</label>
                <input type="text" name="nama_studio" id="nama_studio" class="form-control" placeholder="Masukkan Ruangan Studio Musik" required autocomplete="off" />
                <label for="exampleInputEmail1">Harga Per Sesi</label>
                <input type="text" name="harga" id="harga" class="form-control" placeholder="Masukkan Harga Per Sesi Studio Musik" required autocomplete="off" />
                <label for="exampleInputEmail1">Fasilitas</label>
                <input type="text" name="fasilitas" id="fasilitas" class="form-control" placeholder="Masukkan Fasilitas Studio Musik" required autocomplete="off" />
            </div>

            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Tambah</button>
            </div>
          </div>
        </form>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>



