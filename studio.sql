-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 02, 2020 at 03:42 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `studio`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(5) NOT NULL,
  `nama` varchar(25) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `nama`, `email`, `password`) VALUES
(6, 'nisa', 'nisa@gmail.com', 'b296189270094ab7a79333923193951450d794bb');

-- --------------------------------------------------------

--
-- Table structure for table `jadwalstudiomusik`
--

CREATE TABLE `jadwalstudiomusik` (
  `id_jadwalstudiomusik` int(5) NOT NULL,
  `id_studiomusik` int(5) DEFAULT NULL,
  `id_sesistudiomusikjoin` int(5) DEFAULT NULL,
  `id_user` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jadwalstudiorekaman`
--

CREATE TABLE `jadwalstudiorekaman` (
  `id_jadwalstudiorekaman` int(5) NOT NULL,
  `id_studiorekaman` int(5) DEFAULT NULL,
  `id_sesistudiorekamanjoin` int(5) DEFAULT NULL,
  `id_user` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `peminjamanstudiomusik`
--

CREATE TABLE `peminjamanstudiomusik` (
  `id_peminjamanstudiomusik` int(5) NOT NULL,
  `id_user` int(5) DEFAULT NULL,
  `id_studiomusik` int(5) DEFAULT NULL,
  `namasesi` varchar(10) DEFAULT NULL,
  `id_sesistudiomusik` int(5) DEFAULT NULL,
  `tanggal` datetime NOT NULL,
  `status` text NOT NULL,
  `tgl_submit` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `peminjamanstudiorekaman`
--

CREATE TABLE `peminjamanstudiorekaman` (
  `id_peminjamanstudiorekaman` int(5) NOT NULL,
  `id_user` int(5) DEFAULT NULL,
  `id_studiorekaman` int(5) DEFAULT NULL,
  `id_sesistudiorekaman` int(5) DEFAULT NULL,
  `tanggal` datetime NOT NULL,
  `status` text NOT NULL,
  `tgl_submit` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sesistudiomusik`
--

CREATE TABLE `sesistudiomusik` (
  `id_sesistudiomusik` int(5) NOT NULL,
  `namasesi` varchar(10) NOT NULL,
  `jamakhir` varchar(25) NOT NULL,
  `jammulai` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sesistudiomusik`
--

INSERT INTO `sesistudiomusik` (`id_sesistudiomusik`, `namasesi`, `jamakhir`, `jammulai`) VALUES
(24, 'Sesi 1', '11:00', '09:00'),
(25, 'Sesi 2', '14:00', '12:00');

-- --------------------------------------------------------

--
-- Table structure for table `sesistudiorekaman`
--

CREATE TABLE `sesistudiorekaman` (
  `id_sesistudiorekaman` int(11) NOT NULL,
  `namasesi` varchar(10) NOT NULL,
  `jammulai` varchar(10) NOT NULL,
  `jamakhir` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sesistudiorekaman`
--

INSERT INTO `sesistudiorekaman` (`id_sesistudiorekaman`, `namasesi`, `jammulai`, `jamakhir`) VALUES
(9, 'Sesi 1', '12:00', '14:00');

-- --------------------------------------------------------

--
-- Table structure for table `studiomusik`
--

CREATE TABLE `studiomusik` (
  `id_studiomusik` int(5) NOT NULL,
  `nama_studio` varchar(25) NOT NULL,
  `harga` varchar(10) NOT NULL,
  `fasilitas` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `studiomusik`
--

INSERT INTO `studiomusik` (`id_studiomusik`, `nama_studio`, `harga`, `fasilitas`) VALUES
(10025, 'Studio 1', '30.000', 'Full alat');

-- --------------------------------------------------------

--
-- Table structure for table `studiorekaman`
--

CREATE TABLE `studiorekaman` (
  `id_studiorekaman` int(5) NOT NULL,
  `nama_studio` varchar(25) NOT NULL,
  `harga` varchar(10) NOT NULL,
  `fasilitas` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `studiorekaman`
--

INSERT INTO `studiorekaman` (`id_studiorekaman`, `nama_studio`, `harga`, `fasilitas`) VALUES
(9, 'Studio 1', '40.000', 'Mic, composer, mixer'),
(10, 'Studio 2', '40.000', '	Mic, composer, mixer');

-- --------------------------------------------------------

--
-- Table structure for table `transaksistudiomusik`
--

CREATE TABLE `transaksistudiomusik` (
  `id_transaksistudiomusik` int(5) NOT NULL,
  `total_harga` text NOT NULL,
  `id_peminjamanstudiomusik` int(5) DEFAULT NULL,
  `keterangan` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `transaksistudiorekaman`
--

CREATE TABLE `transaksistudiorekaman` (
  `id_transaksistudiorekaman` int(5) NOT NULL,
  `total_harga` text NOT NULL,
  `id_peminjamanstudiorekaman` int(5) NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(5) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `no_telp` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `nama`, `email`, `password`, `no_telp`) VALUES
(16, 'Annisa Hani Perdana', 'nisa@gmail.com', 'b296189270094ab7a79333923193951450d794bb', '081450247986'),
(18, 'user', 'user@gmail.com', '12dea96fec20593566ab75692c9949596833adc9', '081226103724');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `jadwalstudiomusik`
--
ALTER TABLE `jadwalstudiomusik`
  ADD PRIMARY KEY (`id_jadwalstudiomusik`),
  ADD UNIQUE KEY `id_studiomusik` (`id_studiomusik`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_sesistudiomusik` (`id_sesistudiomusikjoin`);

--
-- Indexes for table `jadwalstudiorekaman`
--
ALTER TABLE `jadwalstudiorekaman`
  ADD PRIMARY KEY (`id_jadwalstudiorekaman`),
  ADD KEY `id_Studiorekaman` (`id_studiorekaman`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_sesistudiorekamanjoin` (`id_sesistudiorekamanjoin`);

--
-- Indexes for table `peminjamanstudiomusik`
--
ALTER TABLE `peminjamanstudiomusik`
  ADD PRIMARY KEY (`id_peminjamanstudiomusik`) USING BTREE,
  ADD KEY `id_studiomusik` (`id_studiomusik`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_sesistudiomusik` (`id_sesistudiomusik`),
  ADD KEY `namasesi` (`namasesi`);

--
-- Indexes for table `peminjamanstudiorekaman`
--
ALTER TABLE `peminjamanstudiorekaman`
  ADD PRIMARY KEY (`id_peminjamanstudiorekaman`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_studiorekaman` (`id_studiorekaman`),
  ADD KEY `id_sesistudiorekaman` (`id_sesistudiorekaman`);

--
-- Indexes for table `sesistudiomusik`
--
ALTER TABLE `sesistudiomusik`
  ADD PRIMARY KEY (`id_sesistudiomusik`);

--
-- Indexes for table `sesistudiorekaman`
--
ALTER TABLE `sesistudiorekaman`
  ADD PRIMARY KEY (`id_sesistudiorekaman`);

--
-- Indexes for table `studiomusik`
--
ALTER TABLE `studiomusik`
  ADD PRIMARY KEY (`id_studiomusik`);

--
-- Indexes for table `studiorekaman`
--
ALTER TABLE `studiorekaman`
  ADD PRIMARY KEY (`id_studiorekaman`);

--
-- Indexes for table `transaksistudiomusik`
--
ALTER TABLE `transaksistudiomusik`
  ADD PRIMARY KEY (`id_transaksistudiomusik`),
  ADD KEY `id_peminjamanstudiomusik` (`id_peminjamanstudiomusik`);

--
-- Indexes for table `transaksistudiorekaman`
--
ALTER TABLE `transaksistudiorekaman`
  ADD PRIMARY KEY (`id_transaksistudiorekaman`),
  ADD KEY `id_peminjamanstudiorekaman` (`id_peminjamanstudiorekaman`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `jadwalstudiomusik`
--
ALTER TABLE `jadwalstudiomusik`
  MODIFY `id_jadwalstudiomusik` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jadwalstudiorekaman`
--
ALTER TABLE `jadwalstudiorekaman`
  MODIFY `id_jadwalstudiorekaman` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `peminjamanstudiomusik`
--
ALTER TABLE `peminjamanstudiomusik`
  MODIFY `id_peminjamanstudiomusik` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `peminjamanstudiorekaman`
--
ALTER TABLE `peminjamanstudiorekaman`
  MODIFY `id_peminjamanstudiorekaman` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `sesistudiomusik`
--
ALTER TABLE `sesistudiomusik`
  MODIFY `id_sesistudiomusik` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `sesistudiorekaman`
--
ALTER TABLE `sesistudiorekaman`
  MODIFY `id_sesistudiorekaman` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `studiomusik`
--
ALTER TABLE `studiomusik`
  MODIFY `id_studiomusik` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10026;

--
-- AUTO_INCREMENT for table `studiorekaman`
--
ALTER TABLE `studiorekaman`
  MODIFY `id_studiorekaman` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `transaksistudiomusik`
--
ALTER TABLE `transaksistudiomusik`
  MODIFY `id_transaksistudiomusik` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `transaksistudiorekaman`
--
ALTER TABLE `transaksistudiorekaman`
  MODIFY `id_transaksistudiorekaman` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `jadwalstudiomusik`
--
ALTER TABLE `jadwalstudiomusik`
  ADD CONSTRAINT `jadwalstudiomusik_ibfk1` FOREIGN KEY (`id_studiomusik`) REFERENCES `studiomusik` (`id_studiomusik`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `jadwalstudiomusik_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `jadwalstudiomusik_ibfk_2` FOREIGN KEY (`id_sesistudiomusikjoin`) REFERENCES `sesistudiomusik` (`id_sesistudiomusik`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `jadwalstudiorekaman`
--
ALTER TABLE `jadwalstudiorekaman`
  ADD CONSTRAINT `jadwalstudiorekaman_ibfk_1` FOREIGN KEY (`id_sesistudiorekamanjoin`) REFERENCES `sesistudiomusik` (`id_sesistudiomusik`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `jadwalstudiorekaman_ibfk_2` FOREIGN KEY (`id_studiorekaman`) REFERENCES `studiorekaman` (`id_studiorekaman`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `jadwalstudiorekaman_ibfk_3` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `peminjamanstudiomusik`
--
ALTER TABLE `peminjamanstudiomusik`
  ADD CONSTRAINT `peminjamanstudiomusik_ibfk_1` FOREIGN KEY (`id_studiomusik`) REFERENCES `studiomusik` (`id_studiomusik`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `peminjamanstudiomusik_ibfk_3` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `peminjamanstudiomusik_ibfk_4` FOREIGN KEY (`id_sesistudiomusik`) REFERENCES `sesistudiomusik` (`id_sesistudiomusik`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `peminjamanstudiorekaman`
--
ALTER TABLE `peminjamanstudiorekaman`
  ADD CONSTRAINT `peminjamanstudiorekaman_ibfk_1` FOREIGN KEY (`id_studiorekaman`) REFERENCES `studiorekaman` (`id_studiorekaman`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `peminjamanstudiorekaman_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `peminjamanstudiorekaman_ibfk_3` FOREIGN KEY (`id_sesistudiorekaman`) REFERENCES `sesistudiorekaman` (`id_sesistudiorekaman`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `transaksistudiomusik`
--
ALTER TABLE `transaksistudiomusik`
  ADD CONSTRAINT `transaksistudiomusik_ibfk_1` FOREIGN KEY (`id_peminjamanstudiomusik`) REFERENCES `peminjamanstudiomusik` (`id_peminjamanstudiomusik`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `transaksistudiorekaman`
--
ALTER TABLE `transaksistudiorekaman`
  ADD CONSTRAINT `transaksistudiorekaman_ibfk_1` FOREIGN KEY (`id_peminjamanstudiorekaman`) REFERENCES `peminjamanstudiorekaman` (`id_peminjamanstudiorekaman`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
